Massmart-pcm Installer Recipes


Local Install
=============

TODO

Docker Images
=============

TODO


BELOW KEPT FOR REFERENCE (Original Readme for b2c accelerator docker image recipe update with the massmart-pcm recipe folder)

Prerequisite
============
Make sure you have the base image installed, if not you can generate the default one following the description under /recipes/base_images/readme.txt.
(the default base image name is ybase_jdk, based on centos and sapjvm, you have to provide).
Instead of generating this image you can select another one by overriding the property 'baseImage; inside your recipe, for your platform/hsql/solr image definition, e.g.:
 property 'baseImage',  'anapsix/alpine-java:8_jdk'

Information
============

Simple setup with b2c accelerator which shows how to create Docker Images of the platform and run them as docker containers
The Images: hsql, solr, zookeeper, loadbalancer, filebeat and platform (with several aspects inside).

To perform follow this scenario:
================================

-> ./install.sh -r massmart-pcm createImagesStructure
--> *Note* To run with solrCloud run ./install.sh -r massmart-pcm createImagesStructure -A solrMode=cloud 
-> cd work/output_images/b2caccDeployment
-> ./build-images.sh
-> cd - && cd recipes/massmart-pcm

-> see hybris-docker-compose/readme for running containers
