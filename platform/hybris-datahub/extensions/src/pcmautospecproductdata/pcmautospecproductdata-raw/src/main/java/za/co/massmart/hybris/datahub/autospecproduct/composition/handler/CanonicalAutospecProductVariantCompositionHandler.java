package za.co.massmart.hybris.datahub.autospecproduct.composition.handler;

import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.CanonicalAttributeModelDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;

import java.util.Iterator;

public class CanonicalAutospecProductVariantCompositionHandler implements
        CompositionRuleHandler {

    private static final String ARTICLE_TYPE = "products-product-articleType";
    private int order;

    public CanonicalAutospecProductVariantCompositionHandler() {
    }

    public CanonicalItem compose(CanonicalAttributeDefinition cad,
                                 CompositionGroup cg, CanonicalItem canonicalItem) {
        CanonicalAttributeModelDefinition camd = cad
                .getCanonicalAttributeModelDefinition();
        Iterator localUnmodifiableIterator = cg.getItems().iterator();
        if (!localUnmodifiableIterator.hasNext()) {
            return canonicalItem;
        }
        RawItem rawItem = (RawItem) localUnmodifiableIterator.next();
        if (rawItem.getField(ARTICLE_TYPE) != null && rawItem.getField(ARTICLE_TYPE).equals("02")) {
            if ("isProduct".equals(camd.getAttributeName())) {
                canonicalItem.setField("isProduct", null);
            } else {
                canonicalItem.setField("isVariant", "yes");
            }
        } else {
            if ("isProduct".equals(camd.getAttributeName())) {
                canonicalItem.setField("isProduct", "yes");
            } else {
                canonicalItem.setField("isVariant", null);
            }
        }
        return canonicalItem;
    }

    public boolean isApplicable(CanonicalAttributeDefinition cad) {
        String rawType = cad.getRawItemType();
        if (!"RawAutospecProductItem".equals(rawType)) {
            return false;
        }
        CanonicalAttributeModelDefinition camd = cad
                .getCanonicalAttributeModelDefinition();
        return ("isProduct".equals(camd.getAttributeName()) || "isVariant".equals(camd.getAttributeName()));
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
        return;
    }

}
