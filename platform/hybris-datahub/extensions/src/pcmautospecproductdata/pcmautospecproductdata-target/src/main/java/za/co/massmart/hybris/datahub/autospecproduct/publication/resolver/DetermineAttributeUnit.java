package za.co.massmart.hybris.datahub.autospecproduct.publication.resolver;

import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.service.CanonicalItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetermineAttributeUnit implements MethodResolver, MethodExecutor {



    private static final String METHOD_NAME = "determineAttributeUnit";

    private CanonicalItemService canonicalItemService;
    private static final Logger LOGGER = LoggerFactory.getLogger("com.mb.datahub.autospecproduct.publication.resolver.DetermineAttributeUnit");

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodExecutor#execute(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.Object[])
     */
    @Override
    public TypedValue execute(final EvaluationContext context, final Object target, final Object... args) throws AccessException
    {

        final CanonicalItem canonicalAutospecProductTemplateAttributeItem = (CanonicalItem) target;

        final String templateCode = (String) canonicalAutospecProductTemplateAttributeItem.getField("templateCode");
        final String templateAttributeCode = (String) canonicalAutospecProductTemplateAttributeItem.getField("templateAttributeCode");

        final DataHubPool pool = canonicalAutospecProductTemplateAttributeItem.getDataPool();
        final Map<String, String> keys = new HashMap<String, String>();

        /**
         * Retrieve characteristic profile via CanonicalBaseArticle based on key pair (productID)
         */
        keys.put("tatemplateCode", templateCode);
        keys.put("taattributeCode", templateAttributeCode);
        final CanonicalItem canonicalAutospecTemplateAttributeItem = canonicalItemService.findLatestValidItem("CanonicalAutospecTemplateAttributeItem", keys,
                pool);
        if (canonicalAutospecTemplateAttributeItem == null) {
            LOGGER.error("Cannot find item for " + templateCode + " " + templateAttributeCode);
            return new TypedValue("");
        }
        String unit = (String) canonicalAutospecTemplateAttributeItem.getField("attributeUOM");
        if (unit == null) {
            unit = "";
        }
        return new TypedValue(unit);
    }


    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodResolver#resolve(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.String, java.util.List)
     */
    @Override
    public MethodExecutor resolve(final EvaluationContext evaluationContext, final Object target, final String method,
                                  final List<TypeDescriptor> argumentTypes) throws AccessException
    {
        if (!METHOD_NAME.equals(method))
        {
            return null;
        }
        return this;
    }

    /**
     * Sets the canonical item service.
     *
     * @param canonicalItemService
     *           the canonical item service
     */
    @Required
    public void setCanonicalItemService(final CanonicalItemService canonicalItemService)
    {
        this.canonicalItemService = canonicalItemService;
    }


}

