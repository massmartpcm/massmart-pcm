package za.co.massmart.hybris.datahub.autospecproduct.publication.resolver;

import com.hybris.datahub.domain.CompositionStatusType;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.paging.DataHubIdBasedPageable;
import com.hybris.datahub.paging.DataHubPage;
import com.hybris.datahub.paging.DefaultDataHubIdBasedPageRequest;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.service.CanonicalItemService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.*;

import java.util.*;

public class DetermineProductFeatures implements MethodResolver, MethodExecutor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DetermineProductFeatures.class.getName());

    private static final String METHOD_NAME = "determineProductFeatures";

    private CanonicalItemService canonicalItemService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodExecutor#execute(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.Object[])
     */
    @Override
    public TypedValue execute(final EvaluationContext context, final Object source, final Object... args) throws AccessException
    {
        String catalogName = (String) args[0];
        String catalogVersion = (String) args[1];
        String productCatalogName = (String) args[2];
        String productCatalogVersion = (String) args[3];

        final CanonicalItem canonicalAutospecProductItem = (CanonicalItem) source;

        String articleID = (String) canonicalAutospecProductItem.getField("articleID");

        final DataHubPool pool = canonicalAutospecProductItem.getDataPool();


        List<String> resultItems = new ArrayList<String>();

        LOGGER.debug("Processing article: " + articleID );
        boolean hasRecords = true;
        long id = 0L;
        while (hasRecords) {
            DataHubIdBasedPageable dataHubImportBasedPageable = new DefaultDataHubIdBasedPageRequest(100, id);
            DataHubPage<CanonicalItem> importPage = canonicalItemService.findCanonicalItems(pool, "CanonicalAutospecProductTemplateAttributeItem", dataHubImportBasedPageable, CompositionStatusType.SUCCESS);
            if (importPage.getContent().size() > 0) {
                List<CanonicalItem> importItems = importPage.getContent();
                for (int i=0;i<importItems.size();i++) {
                    CanonicalItem importItem = importItems.get(i);
                    String matchArticleID = (String) importItem.getField("articleID");
                    if (matchArticleID.equals(articleID)) {
                        final String templateCode = (String) importItem.getField("templateCode");
                        final String templateAttributeCode = (String) importItem.getField("templateAttributeCode");

                        String result = catalogName + "/" + catalogVersion + "/" + templateCode + "." + templateAttributeCode + ":" + StringUtils.leftPad(matchArticleID, 18, "0") + ":" + productCatalogName + ":" + productCatalogVersion;
                        resultItems.add(result);
                    }
                    id = importItem.getId();
                }
            } else {
                hasRecords = false;
            }
        }

        hasRecords = true;
        id = 0L;
        while (hasRecords) {
            DataHubIdBasedPageable dataHubImportBasedPageable = new DefaultDataHubIdBasedPageRequest(100, id);
            DataHubPage<CanonicalItem> importPage = canonicalItemService.findCanonicalItems(pool, "CanonicalAutospecProductTemplateAttributeComplexItem", dataHubImportBasedPageable, CompositionStatusType.SUCCESS);
            if (importPage.getContent().size() > 0) {
                List<CanonicalItem> importItems = importPage.getContent();
                for (int i=0;i<importItems.size();i++) {
                    CanonicalItem importItem = importItems.get(i);
                    String matchArticleID = (String) importItem.getField("articleID");
                    if (matchArticleID.equals(articleID)) {
                        final String templateCode = (String) importItem.getField("templateCode");
                        final String templateAttributeCode = (String) importItem.getField("templateAttributeCode");
                        final Collection<String> templateAttributeValues = (Collection<String>) importItem.getField("templateAttributeValue");
                        for (String value:templateAttributeValues) {
                            String result = catalogName + "/" + catalogVersion + "/" + templateCode + "." + templateAttributeCode + "." + value + ":" + matchArticleID + ":" + productCatalogName + ":" + productCatalogVersion;
                            if (!resultItems.contains(result)) {
                                resultItems.add(result);
                            }
                        }
                    }
                    id = importItem.getId();
                }
            } else {
                hasRecords = false;
            }
        }

        return new TypedValue(resultItems);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodResolver#resolve(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.String, java.util.List)
     */
    @Override
    public MethodExecutor resolve(final EvaluationContext evaluationContext, final Object target, final String method,
                                  final List<TypeDescriptor> argumentTypes) throws AccessException
    {
        if (!METHOD_NAME.equals(method))
        {
            return null;
        }
//		if (argumentTypes == null || !argumentTypes.isEmpty())
//		{
//			return null;
//		}
        return this;
    }

    /**
     * Sets the canonical item service.
     *
     * @param canonicalItemService
     *           the canonical item service
     */
    @Required
    public void setCanonicalItemService(final CanonicalItemService canonicalItemService)
    {
        this.canonicalItemService = canonicalItemService;
    }

}