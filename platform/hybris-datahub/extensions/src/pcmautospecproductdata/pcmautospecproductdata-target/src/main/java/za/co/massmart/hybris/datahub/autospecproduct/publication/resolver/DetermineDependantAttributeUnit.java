package za.co.massmart.hybris.datahub.autospecproduct.publication.resolver;

import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.service.CanonicalItemService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetermineDependantAttributeUnit implements MethodResolver, MethodExecutor {



    private static final String METHOD_NAME = "determineDependantAttributeUnit";

    private CanonicalItemService canonicalItemService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodExecutor#execute(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.Object[])
     */
    @Override
    public TypedValue execute(final EvaluationContext context, final Object target, final Object... args) throws AccessException
    {

        final CanonicalItem canonicalAutospecProductTemplateAttributeDependantItem = (CanonicalItem) target;

        final String templateCode = (String) canonicalAutospecProductTemplateAttributeDependantItem.getField("templateCode");
        final String templateAttributeCode = (String) canonicalAutospecProductTemplateAttributeDependantItem.getField("templateAttributeCode");
        final String templateDependantAttributeCode = (String) canonicalAutospecProductTemplateAttributeDependantItem.getField("templateDependantAttributeCode");

        final DataHubPool pool = canonicalAutospecProductTemplateAttributeDependantItem.getDataPool();
        final Map<String, String> keys = new HashMap<>();

        /**
         * Retrieve characteristic profile via CanonicalBaseArticle based on key pair (productID)
         */
        keys.put("tadatemplateCode", templateCode);
        keys.put("tadaattributeCode", templateAttributeCode);
        keys.put("dependantAttributeCode", templateDependantAttributeCode);
        final CanonicalItem canonicalAutospecTemplateAttributeItem = canonicalItemService.findLatestValidItem("CanonicalAutospecTemplateAttributeDependantAttributeItem", keys,
                pool);
        String unit = "";
        if (canonicalAutospecTemplateAttributeItem != null) {
            unit = (String) canonicalAutospecTemplateAttributeItem.getField("dependantAttributeUOM");
            if (unit == null) {
                unit = "";
            }
        }

        return new TypedValue(unit);
    }


    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodResolver#resolve(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.String, java.util.List)
     */
    @Override
    public MethodExecutor resolve(final EvaluationContext evaluationContext, final Object target, final String method,
                                  final List<TypeDescriptor> argumentTypes) throws AccessException
    {
        if (!METHOD_NAME.equals(method))
        {
            return null;
        }
        return this;
    }

    /**
     * Sets the canonical item service.
     *
     * @param canonicalItemService
     *           the canonical item service
     */
    @Required
    public void setCanonicalItemService(final CanonicalItemService canonicalItemService)
    {
        this.canonicalItemService = canonicalItemService;
    }


}

