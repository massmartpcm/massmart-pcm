package za.co.massmart.hybris.datahub.autospecproduct.publication;

import com.hybris.datahub.grouping.TargetItemCreationContext;
import com.hybris.datahub.grouping.impl.ConfigurablePublicationGroupingHandler;
import com.hybris.datahub.model.CanonicalItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ClassificationAttributeValuePublicationHandler extends ConfigurablePublicationGroupingHandler
{

    private static final Logger LOGGER = LoggerFactory.getLogger("za.co.massmart.hybris.datahub.autospecproduct.publication.ClassificationAttributeValuePublicationHandler");

    public ClassificationAttributeValuePublicationHandler()
    {
        setOrder(100);
    }

    public List<CanonicalItem> group(CanonicalItem item, TargetItemCreationContext context)
    {
        Collection<String> values = (Collection)item.getField("templateAttributeValue");
        if(values == null || values.isEmpty())
        {
            return Collections.emptyList();
        }
        List<CanonicalItem> list = new ArrayList<>(values.size());
        CanonicalItem clone;
        for(String value:values)
        {
            clone = CanonicalItem.copyOf(item);
            clone.setField("templateAttributeSingleValue", value);
            list.add(clone);
        }

        return list;
    }

    public boolean isApplicable(CanonicalItem item, TargetItemCreationContext context)
    {
        return "TargetAutospecProductFeatureComplexItem".equals(context.getTargetItemTypeCode());
    }

}

