package za.co.massmart.datahub.media.composition.handler;

import java.util.Iterator;

import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.CanonicalAttributeModelDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;

public class CanonicalSiloProductVariantCompositionHandler implements
		CompositionRuleHandler {

	private static final String ARTICLE_TYPE = "data-Import-attyp";
	private int order;

	public CanonicalSiloProductVariantCompositionHandler() {
	}

	public CanonicalItem compose(CanonicalAttributeDefinition cad,
			CompositionGroup cg, CanonicalItem canonicalItem) {
		CanonicalAttributeModelDefinition camd = cad
				.getCanonicalAttributeModelDefinition();
		Iterator localUnmodifiableIterator = cg.getItems().iterator();
		if (!localUnmodifiableIterator.hasNext()) {
			return canonicalItem;
		}
		RawItem rawItem = (RawItem) localUnmodifiableIterator.next();
		if (rawItem.getField(ARTICLE_TYPE) != null && rawItem.getField(ARTICLE_TYPE).equals("2")) {
			if ("isProduct".equals(camd.getAttributeName())) {
				canonicalItem.setField("isProduct",null);
			} else {
				canonicalItem.setField("isVariant","yes");
			}
		} else {
			if ("isProduct".equals(camd.getAttributeName())) {
				canonicalItem.setField("isProduct","yes");
			} else {
				canonicalItem.setField("isVariant",null);
			}
		}
		return canonicalItem;
	}

	public boolean isApplicable(CanonicalAttributeDefinition cad) {
		String rawType = cad.getRawItemType();
		if (!"RawSiloMedia".equals(rawType)) {
			return false;
		}
		CanonicalAttributeModelDefinition camd = cad
				.getCanonicalAttributeModelDefinition();

		return ("isProduct".equals(camd.getAttributeName()) || "isVariant".equals(camd.getAttributeName()));
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

}
