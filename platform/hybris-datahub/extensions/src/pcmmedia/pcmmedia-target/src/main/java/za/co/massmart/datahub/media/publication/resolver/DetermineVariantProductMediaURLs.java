package za.co.massmart.datahub.media.publication.resolver;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.MethodExecutor;
import org.springframework.expression.MethodResolver;
import org.springframework.expression.TypedValue;

import com.hybris.datahub.domain.CompositionStatusType;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.paging.DataHubIdBasedPageable;
import com.hybris.datahub.paging.DefaultDataHubIdBasedPageRequest;
import com.hybris.datahub.paging.DataHubPage;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.service.CanonicalItemService;


/**
 * Determines the URLs of the images for the article.
 *
 *
 * @author Chris
 * @version 1.0
 *
 */
public class DetermineVariantProductMediaURLs implements MethodResolver, MethodExecutor
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DetermineVariantProductMediaURLs.class.getName());

	private static final String METHOD_NAME = "determineVariantProductMediaURLs";

	private CanonicalItemService canonicalItemService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.expression.MethodExecutor#execute(org.springframework.expression.EvaluationContext,
	 * java.lang.Object, java.lang.Object[])
	 */
	@Override
	public TypedValue execute(final EvaluationContext context, final Object source, final Object... args) throws AccessException
	{
		final CanonicalItem siloImportCanonicalItem = (CanonicalItem) source;

		String articleNumber = (String) siloImportCanonicalItem.getField("articleNumber");

		final DataHubPool pool = siloImportCanonicalItem.getDataPool();

		List<String> resultItems = new ArrayList<>();

		boolean importPageHasRecords = true;
		long importPageId = 0L; 
		while (importPageHasRecords) {
			DataHubIdBasedPageable dataHubImportBasedPageable = new DefaultDataHubIdBasedPageRequest(100, importPageId);
			DataHubPage<CanonicalItem> importPage = canonicalItemService.findCanonicalItems(pool, "SiloImportCanonicalVariantItem", dataHubImportBasedPageable, CompositionStatusType.SUCCESS);

			if (importPage.getContent().size() > 0) {
			
				List<CanonicalItem> importItems = importPage.getContent();
				for (CanonicalItem importItem : importItems) {
					String itemArticleNumber = (String)importItem.getField("articleNumber");
					
					if (articleNumber.equals(itemArticleNumber)) {
						String importId = (String)importItem.getField("importId");

						boolean pageHasRecords = true;
						long pageId = 0L; 
						while (pageHasRecords) {

							DataHubIdBasedPageable dataHubImageBasedPageable = new DefaultDataHubIdBasedPageRequest(100, pageId);
							DataHubPage<CanonicalItem> page = canonicalItemService.findCanonicalItems(pool, "SiloMediaCanonicalItem", dataHubImageBasedPageable, CompositionStatusType.SUCCESS);
						
							if (page.getContent().size() > 0) {
								List<CanonicalItem> items = page.getContent();
								for (CanonicalItem item : items) {
									String itemImportId = (String)item.getField("importId");
									if (itemImportId.equals(importId)) {
										String imageId = (String)item.getField("imageId");
										String sequenceId = (String)item.getField("sequenceId");
										String imageView = (String)item.getField("imageView");
										String result = sequenceId + "-" + imageId + "-" + imageView + "-container";
										resultItems.add(result);
									}
									pageId = item.getId();
								}
							} else {
								pageHasRecords = false;
							}
						}
					}
					importPageId = importItem.getId();
				}
			} else {
				importPageHasRecords = false;
			}
		}
		return new TypedValue(resultItems);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.expression.MethodResolver#resolve(org.springframework.expression.EvaluationContext,
	 * java.lang.Object, java.lang.String, java.util.List)
	 */
	@Override
	public MethodExecutor resolve(final EvaluationContext evaluationContext, final Object target, final String method,
			final List<TypeDescriptor> argumentTypes) throws AccessException
	{
		if (!METHOD_NAME.equals(method))
		{
			return null;
		}
//		if (argumentTypes == null || !argumentTypes.isEmpty())
//		{
//			return null;
//		}
		return this;
	}

	/**
	 * Sets the canonical item service.
	 *
	 * @param canonicalItemService
	 *           the canonical item service
	 */
	@Required
	public void setCanonicalItemService(final CanonicalItemService canonicalItemService)
	{
		this.canonicalItemService = canonicalItemService;
	}

}
