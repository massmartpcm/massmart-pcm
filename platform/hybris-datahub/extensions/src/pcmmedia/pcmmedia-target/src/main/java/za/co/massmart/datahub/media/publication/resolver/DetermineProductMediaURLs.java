package za.co.massmart.datahub.media.publication.resolver;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.MethodExecutor;
import org.springframework.expression.MethodResolver;
import org.springframework.expression.TypedValue;

import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import za.co.massmart.datahub.service.CanonicalItemService;


/**
 * Determines the URLs of the images for the article.
 *
 *
 * @author Chris
 * @version 1.0
 *
 */
public class DetermineProductMediaURLs implements MethodResolver, MethodExecutor
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DetermineProductMediaURLs.class.getName());

	private static final String METHOD_NAME = "determineProductMediaURLs";

	private CanonicalItemService canonicalItemService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.expression.MethodExecutor#execute(org.springframework.expression.EvaluationContext,
	 * java.lang.Object, java.lang.Object[])
	 */
	@Override
	public TypedValue execute(final EvaluationContext context, final Object source, final Object... args) throws AccessException
	{
		final CanonicalItem siloImportCanonicalItem = (CanonicalItem) source;

		String articleNumber = (String) siloImportCanonicalItem.getField("articleNumber");

		final DataHubPool pool = siloImportCanonicalItem.getDataPool();

		List<String> resultItems = new ArrayList<String>();

		Collection<CanonicalItem> importItems = canonicalItemService.findItemsByPartOfIntegrationKey("SiloImportCanonicalItem", articleNumber, pool);

		for (CanonicalItem importItem:importItems) {
			String importId = (String)importItem.getField("importId");

			Collection<CanonicalItem> imageItems = canonicalItemService.findItemsByPartOfIntegrationKey("SiloMediaCanonicalItem", importId, pool);

			for (CanonicalItem imageItem:imageItems) {
				String imageId = (String)imageItem.getField("b_imageId");
				String sequenceId = (String)imageItem.getField("sequenceId");
				String imageView = (String)imageItem.getField("imageView");
				String result = sequenceId + "-" + imageId + "-" + imageView + "-container";
				resultItems.add(result);
			}
		}

//		boolean importPageHasRecords = true;
//		long importPageId = 0L; 
//		while (importPageHasRecords) {
//			DataHubIdBasedPageable dataHubImportBasedPageable = new DataHubIdBasedPageableImpl(importPageId,100);
//			DataHubPage<CanonicalItem> importPage = canonicalItemService.findCanonicalItems(pool, "SiloImportCanonicalItem", dataHubImportBasedPageable, CompositionStatusType.SUCCESS);
//
//			if (importPage.getContent().size() > 0) {
//			
//				List<CanonicalItem> importItems = importPage.getContent();
//				for (int i=0;i<importItems.size();i++) {
//					CanonicalItem importItem = importItems.get(i);
//					String itemArticleNumber = (String)importItem.getField("articleNumber");
//					
//					if (articleNumber.equals(itemArticleNumber)) {
//						String importId = (String)importItem.getField("importId");
//
//						boolean pageHasRecords = true;
//						long pageId = 0L; 
//						while (pageHasRecords) {
//
//							DataHubIdBasedPageable dataHubImageBasedPageable = new DataHubIdBasedPageableImpl(pageId,100);
//							DataHubPage<CanonicalItem> page = canonicalItemService.findCanonicalItems(pool, "SiloMediaCanonicalItem", dataHubImageBasedPageable, CompositionStatusType.SUCCESS);
//						
//							if (page.getContent().size() > 0) {
//								List<CanonicalItem> items = page.getContent();
//								for (int j=0;j<items.size();j++) {
//									CanonicalItem item = items.get(j);
//									String itemImportId = (String)item.getField("importId");
//									if (itemImportId.equals(importId)) {
//										String imageId = (String)item.getField("imageId");
//										String sequenceId = (String)item.getField("sequenceId");
//										String imageView = (String)item.getField("imageView");
//										String result = sequenceId + "-" + imageId + "-" + imageView + "-container";
//										resultItems.add(result);
//									}
//									pageId = item.getId();
//								}
//							} else {
//								pageHasRecords = false;
//							}
//						}
//					}
//					importPageId = importItem.getId();
//				}
//			} else {
//				importPageHasRecords = false;
//			}
//		}
		return new TypedValue(resultItems);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.expression.MethodResolver#resolve(org.springframework.expression.EvaluationContext,
	 * java.lang.Object, java.lang.String, java.util.List)
	 */
	@Override
	public MethodExecutor resolve(final EvaluationContext evaluationContext, final Object target, final String method,
			final List<TypeDescriptor> argumentTypes) throws AccessException
	{
		if (!METHOD_NAME.equals(method))
		{
			return null;
		}
//		if (argumentTypes == null || !argumentTypes.isEmpty())
//		{
//			return null;
//		}
		return this;
	}

	/**
	 * Sets the canonical item service.
	 *
	 * @param canonicalItemService
	 *           the canonical item service
	 */
	@Required
	public void setCanonicalItemService(final CanonicalItemService canonicalItemService)
	{
		this.canonicalItemService = canonicalItemService;
	}

}
