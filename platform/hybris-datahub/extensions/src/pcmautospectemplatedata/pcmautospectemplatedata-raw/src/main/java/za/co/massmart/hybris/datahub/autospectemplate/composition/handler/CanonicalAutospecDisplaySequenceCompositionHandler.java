package za.co.massmart.hybris.datahub.autospectemplate.composition.handler;

import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.CanonicalAttributeModelDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;

import java.util.Iterator;

public class CanonicalAutospecDisplaySequenceCompositionHandler implements CompositionRuleHandler {

    private static final String DISPLAY_SEQUENCE = "templates-template-attributes-attribute-displaySequence";
    private int order;

    public CanonicalAutospecDisplaySequenceCompositionHandler() {
    }

    public CanonicalItem compose(CanonicalAttributeDefinition cad, CompositionGroup cg, CanonicalItem canonicalItem) {
        CanonicalAttributeModelDefinition camd = cad.getCanonicalAttributeModelDefinition();
        Iterator localUnmodifiableIterator = cg.getItems().iterator();
        do {
            if (!localUnmodifiableIterator.hasNext()) {
                break;
            }
            RawItem rawItem = (RawItem) localUnmodifiableIterator.next();
            if (rawItem.getField(DISPLAY_SEQUENCE) == null) {
                canonicalItem.setField("attributeDisplaySequence", " ");
            } else {
                canonicalItem.setField("attributeDisplaySequence", rawItem.getField(DISPLAY_SEQUENCE));
            }
        } while (true);
        return canonicalItem;
    }

    public boolean isApplicable(CanonicalAttributeDefinition cad) {
        String rawType = cad.getRawItemType();
        if (!"RawAutospecTemplateItem".equals(rawType)) {
            return false;
        }
        CanonicalAttributeModelDefinition camd = cad.getCanonicalAttributeModelDefinition();
        if (!camd.getCanonicalItemMetadata().getItemType().equals("CanonicalAutospecTemplateAttributeAssignmentItem")) {
            return false;
        }
        if (!"attributeDisplaySequence".equals(camd.getAttributeName())) {
            return false;
        }

        return true;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
        return;
    }

}
