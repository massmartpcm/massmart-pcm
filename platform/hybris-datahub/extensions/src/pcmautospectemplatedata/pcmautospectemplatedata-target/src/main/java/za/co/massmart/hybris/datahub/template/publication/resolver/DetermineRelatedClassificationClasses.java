package za.co.massmart.hybris.datahub.template.publication.resolver;

import com.hybris.datahub.domain.CompositionStatusType;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.paging.DataHubIdBasedPageable;
import com.hybris.datahub.paging.DataHubPage;
import com.hybris.datahub.paging.DefaultDataHubIdBasedPageRequest;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.service.CanonicalItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.*;

import java.util.ArrayList;
import java.util.List;

public class DetermineRelatedClassificationClasses implements MethodResolver, MethodExecutor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DetermineRelatedClassificationClasses.class.getName());

    private static final String METHOD_NAME = "determineRelatedClassificationClasses";

    private CanonicalItemService canonicalItemService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodExecutor#execute(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.Object[])
     */
    @Override
    public TypedValue execute(final EvaluationContext context, final Object source, final Object... args) throws AccessException {
        final CanonicalItem canonicalAutospecTemplateRelatedTemplateItem = (CanonicalItem) source;

        String trttemplateCode = (String) canonicalAutospecTemplateRelatedTemplateItem.getField("trttemplateCode");

        final DataHubPool pool = canonicalAutospecTemplateRelatedTemplateItem.getDataPool();

        List<String> resultItems = new ArrayList<String>();

        boolean hasRecords = true;
        long id = 0L;
        while (hasRecords) {

            DataHubIdBasedPageable dataHubImportBasedPageable = new DefaultDataHubIdBasedPageRequest(100, id);
            DataHubPage<CanonicalItem> importPage = canonicalItemService.findCanonicalItems(pool, "CanonicalAutospecTemplateRelatedTemplateItem", dataHubImportBasedPageable, CompositionStatusType.SUCCESS);

            if (importPage.getContent().size() > 0) {
                List<CanonicalItem> importItems = importPage.getContent();
                for (int i = 0; i < importItems.size(); i++) {
                    CanonicalItem importItem = importItems.get(i);
                    String templateCode = (String) importItem.getField("trttemplateCode");
                    if (trttemplateCode.equals(templateCode)) {
                        String relatedTemplateCode = (String) importItem.getField("trtrelatedTemplateCode");
                        resultItems.add(templateCode + "_" + relatedTemplateCode + ":" + args[1] + ":" + args[2]);
                    }
                    id = importItem.getId();
                }
            } else {
                hasRecords = false;
            }
        }

        return new TypedValue(resultItems);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.expression.MethodResolver#resolve(org.springframework.expression.EvaluationContext,
     * java.lang.Object, java.lang.String, java.util.List)
     */
    @Override
    public MethodExecutor resolve(final EvaluationContext evaluationContext, final Object target, final String method,
                                  final List<TypeDescriptor> argumentTypes) throws AccessException {
        if (!METHOD_NAME.equals(method)) {
            return null;
        }
        return this;
    }

    /**
     * Sets the canonical item service.
     *
     * @param canonicalItemService the canonical item service
     */
    @Required
    public void setCanonicalItemService(final CanonicalItemService canonicalItemService) {
        this.canonicalItemService = canonicalItemService;
    }
}