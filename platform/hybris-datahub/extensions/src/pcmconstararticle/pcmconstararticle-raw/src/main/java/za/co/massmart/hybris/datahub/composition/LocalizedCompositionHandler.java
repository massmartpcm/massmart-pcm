package za.co.massmart.hybris.datahub.composition;

import com.google.common.collect.UnmodifiableIterator;
import com.hybris.datahub.composition.CompositionRuleHandler;
import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.domain.CanonicalAttributeModelDefinition;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class LocalizedCompositionHandler implements CompositionRuleHandler {
    private static final Function<String, Locale> LOCALE_NEW = Locale::new;
    private static final ConcurrentHashMap<String, Locale> LOCALES = new ConcurrentHashMap();
    private static final AtomicInteger ORDER_PROVIDER = new AtomicInteger(1000);
    private String canonicalAttribute;
    private String canonicalTypeCode;
    private int order;
    private String rawAttribute;
    private String languageISO;
    private String rawTypeCode;

    public LocalizedCompositionHandler() {
        this.order = ORDER_PROVIDER.getAndIncrement();
    }

    public <T extends CanonicalItem> T compose(CanonicalAttributeDefinition cad, CompositionGroup<? extends RawItem> cg, T canonicalItem) {
        UnmodifiableIterator var4 = cg.getItems().iterator();

        while(var4.hasNext()) {
            RawItem rawItem = (RawItem)var4.next();
            Object value = rawItem.getField(this.rawAttribute);
            String locale = languageISO;
            if (value != null && locale != null && !locale.isEmpty()) {
                canonicalItem.setField(this.canonicalAttribute, value, (Locale)LOCALES.computeIfAbsent(locale, LOCALE_NEW));
            }
        }

        return canonicalItem;
    }

    public int getOrder() {
        return this.order;
    }

    public boolean isApplicable(CanonicalAttributeDefinition cad) {
        if (!this.rawTypeCode.equals(cad.getRawItemType())) {
            return false;
        } else {
            CanonicalAttributeModelDefinition camd = cad.getCanonicalAttributeModelDefinition();
            return this.canonicalAttribute.equals(camd.getAttributeName()) && this.canonicalTypeCode.equals(camd.getCanonicalItemMetadata().getItemType());
        }
    }

    @Required
    public void setCanonicalAttribute(String canonicalAttribute) {
        assert canonicalAttribute != null : canonicalAttribute;

        assert !canonicalAttribute.isEmpty() : canonicalAttribute;

        this.canonicalAttribute = canonicalAttribute.intern();
    }

    @Required
    public void setCanonicalTypeCode(String canonicalTypeCode) {
        assert canonicalTypeCode != null : canonicalTypeCode;

        assert !canonicalTypeCode.isEmpty() : canonicalTypeCode;

        this.canonicalTypeCode = canonicalTypeCode.intern();
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Required
    public void setRawAttribute(String rawAttribute) {
        assert rawAttribute != null : rawAttribute;

        assert !rawAttribute.isEmpty() : rawAttribute;

        this.rawAttribute = rawAttribute.intern();
    }

    @Required
    public void setLanguageISO(String languageISO) {
        assert languageISO != null : languageISO;

        assert !languageISO.isEmpty() : languageISO;

        this.languageISO = languageISO.intern();
    }

    @Required
    public void setRawTypeCode(String rawTypeCode) {
        assert rawTypeCode != null : rawTypeCode;

        assert !rawTypeCode.isEmpty() : rawTypeCode;

        this.rawTypeCode = rawTypeCode.intern();
    }
}

