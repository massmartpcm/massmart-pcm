package za.co.massmart.datahub.pcm3rdpartyintegration.spring;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;


/**
 * Service to receive XML docs from 3rd Party systems.
 */
@Component
@Path("/xml/receiver/")
public class ThirdpartyHttpInboundService
{
	private static final class XMLCount
	{
		private final String tag;
		protected final AtomicLong count = new AtomicLong();

		protected XMLCount(String tag)
		{
			this.tag = tag;
		}

		@Override
		public int hashCode()
		{
			return tag.hashCode();
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof XMLCount))
				return false;
			XMLCount other = (XMLCount) obj;
			return tag.equals(other.tag);
		}

		@Override
		public String toString()
		{
			StringBuilder builder = new StringBuilder();
			builder.append(tag);
			builder.append(' ');
			builder.append(count);
			return builder.toString();
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdpartyHttpInboundService.class);
	private static final ConcurrentMap<String, XMLCount> xmlDOCS = new ConcurrentHashMap<>();
	private static final Pattern REGEX_TAG = Pattern.compile("^[/A-Za-z0-9_]+$");
	private MessageChannel idocXmlInboundChannel;

	/**
	 * Receives XML DOCs and sends Spring-Integration xml messages for them. The RFC destination in sm59 has to be
	 * maintained as follows
	 * 
	 * <pre>
	 * Target Host: your full qualified computer name
	 * Port: 8080
	 * Path Prefix: /datahub-webapp/v1/xml/receiver
	 * </pre>
	 * 
	 * @param idoc
	 *           tag XML
	 * @return {@link Response}
	 */
	@POST
	@Consumes(
	{ MediaType.APPLICATION_XML, MediaType.TEXT_XML })
	public Response receiveXMLdoc(String xml, @Context HttpHeaders headers)
	{
		try
		{
			final long start = System.currentTimeMillis();
			LOGGER.debug("XMLdocintegration - Received XML :\r\n{}", xml);
			final String xmlType = getXMLType(xml);
			String docType = headers.getRequestHeader("client_identifier").get(0);
			final Map<String, Object> messHeaders = Collections.singletonMap("IDOCTYP", docType);
			final GenericMessage<String> message = new GenericMessage<>(xml, messHeaders);
			idocXmlInboundChannel.send(message);
			final long end = System.currentTimeMillis();
			LOGGER.info("The processing of the {}({}) tag took {} milliseconds.", xmlType, docType, (end - start));

			ThirdpartyHttpInboundService.incrementXMLCount(xmlType);
		}
		catch (final Exception ex)
		{
			LOGGER.error("Exception occurred during processing of tag : {}", xml, ex);
			throw ex;
		}
		return Response.ok().build();
	}

	private static void incrementXMLCount(final String xmlType)
	{
		XMLCount xmldocCount = xmlDOCS.get(xmlType);
		if (xmldocCount == null)
		{
			String xmlTypeIntern = xmlType.intern();
			xmldocCount = new XMLCount(xmlTypeIntern);
			XMLCount putIfAbsent = xmlDOCS.putIfAbsent(xmlTypeIntern, xmldocCount);
			if (putIfAbsent != null)
			{
				xmldocCount = putIfAbsent;
			}
		}
		xmldocCount.count.incrementAndGet();
	}

	/**
	 * Receives connection test from IDoc sender.
	 * 
	 * @return 200 OK {@link Response} if IDoc receiver is running.
	 */
	@GET
	@Produces(
	{ MediaType.TEXT_PLAIN })
	public Response receiveConnectionTest()
	{
		LOGGER.debug("Received GET request for connection test.");

		StringBuilder sb = new StringBuilder();
		sb.append("XMLDoc receiver is running.");
		sb.append('\n');
		String idocs = xmlDOCS.values().stream() //
				.map(XMLCount::toString) //
				.sorted() //
				.collect(Collectors.joining("\n"));
		sb.append(idocs);
		return Response.ok(sb.toString()).build();
	}

	/**
	 * Retrieves the IDOCTYPE from the xml message.
	 * 
	 * @param idoc
	 *           the xml of the received tag
	 * @return the message type of the tag
	 */
	protected String getIdocType(final String idoc)
	{
		final int start = idoc.indexOf("<IDOCTYP>", 3) + "<IDOCTYP>".length();
		final int end = idoc.indexOf("</IDOCTYP>", start);
		return idoc.substring(start, end);
	}

	protected String getXMLType(final String idoc)
	{
		final int start = idoc.indexOf("<", 1) + 1;
		final int end = idoc.indexOf(">", start);
		String substring = idoc.substring(start, end);
		if (REGEX_TAG.matcher(substring).matches())
		{
			return substring;
		}
		LOGGER.error("Invalid xml tag : ", substring);
		return "INVALID";
	}

	/**
	 * Channel into which received IDOCs are put.
	 * 
	 * @param idocXmlInboundChannel
	 *           {@link MessageChannel} of the 'idocXmlInboundChannel'
	 */
	@Required
	public void setIdocXmlInboundChannel(final MessageChannel idocXmlInboundChannel)
	{
		this.idocXmlInboundChannel = idocXmlInboundChannel;
	}
}
