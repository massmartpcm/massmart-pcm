package za.co.massmart.datahub.grouping;

import com.hybris.datahub.domain.CanonicalAttributeDefinition;
import com.hybris.datahub.model.CompositionGroup;
import com.hybris.datahub.model.RawItem;
import com.hybris.datahub.sapidocintegration.grouping.FilterAbstractGroupingHandler;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FilterKeysGroupingHandler extends FilterAbstractGroupingHandler
{
    private static final Predicate<Object> IS_NULL_OR_EMPTY = o -> o == null || ((String) o).isEmpty();

    private static final AtomicInteger ORDER_PROVIDER = new AtomicInteger(100000);
    private ArrayList<String> rawItemAttributes;

    public FilterKeysGroupingHandler()
    {
        this.order = ORDER_PROVIDER.getAndIncrement();
    }

    @Override
    public <T extends RawItem> boolean isApplicable(CompositionGroup<T> cg)
    {
        assert cg != null;
        List<T> items = cg.getItems();
        assert items != null;
        assert !items.isEmpty();
        if (!this.rawItemType.equals(items.get(0).getType()))
        {
            return false;
        }
        List<CanonicalAttributeDefinition> attributes = cg.getAttributes();
        assert attributes != null;
        assert !attributes.isEmpty();
        if (!this.canonicalItemType.equals(attributes.get(0).getCanonicalAttributeModelDefinition().getCanonicalItemMetadata()
                .getItemType()))
        {
            return false;
        }

        return items.stream() //
                .flatMap(r -> rawItemAttributes.stream().map(r::getField)) //
                .anyMatch(IS_NULL_OR_EMPTY);
    }

    /**
     * Group of Raw Item Attributes which is mandatory to compose a complete integration key.
     *
     * @param rawItemAttributes
     *           a {@link Set} of {@link String}.
     * @see RawItem#getAttributeDefinition(String, String)
     */
    @Required
    public void setRawItemAttributes(final Set<String> rawItemAttributes)
    {
        assert rawItemAttributes != null : rawItemAttributes;
        assert !rawItemAttributes.isEmpty() : rawItemAttributes;
        this.rawItemAttributes = rawItemAttributes.stream() //
                .map(String::intern) //
                .collect(Collectors.toCollection(ArrayList::new));
        this.rawItemAttributes.trimToSize();
    }
}