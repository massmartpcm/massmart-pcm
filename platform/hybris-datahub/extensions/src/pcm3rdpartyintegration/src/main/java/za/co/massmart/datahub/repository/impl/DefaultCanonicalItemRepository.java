package za.co.massmart.datahub.repository.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;
import com.hybris.datahub.domain.CompositionStatusType;
import com.hybris.datahub.domain.jpa.entities.ManagedCanonicalItemEntity;
import com.hybris.datahub.domain.jpa.entities.QManagedCanonicalItemEntity;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.persistence.jpa.util.YJpaRepository;
import com.hybris.datahub.repository.converter.CanonicalItemConverter;
import com.hybris.datahub.repository.jpa.impl.DefaultCanonicalItemJpaRepository;
import com.hybris.datahub.repository.jpa.predicates.CanonicalItemPredicates;
import com.hybris.datahub.runtime.domain.DataHubPool;
import com.hybris.datahub.runtime.domain.jpa.entities.DataHubPoolEntity;
import za.co.massmart.datahub.repository.CanonicalItemRepository;


public class DefaultCanonicalItemRepository extends DefaultCanonicalItemJpaRepository implements CanonicalItemRepository {

	private YJpaRepository<ManagedCanonicalItemEntity> jpaRepository;
	private CanonicalItemConverter converter;

	public List<CanonicalItem> findByPoolAndPartOfIntegrationKey(DataHubPool pool, String intKey, String type,
																 CompositionStatusType... statuses) {
		Iterable<ManagedCanonicalItemEntity> all = this.jpaRepository.findAll(
				this.buildPoolTypeStatuesAndPartOfIntegrationKeyPredicate(pool, intKey, type, statuses),
				new OrderSpecifier[]{CanonicalItemPredicates.ORDER_CANONICAL_ITEM_ID_DESC});
		return this.convertQueryResults(all);
	}

	private BooleanExpression buildPoolTypeStatuesAndPartOfIntegrationKeyPredicate(DataHubPool pool, String intKey,
																				   String type, CompositionStatusType[] statuses) {

		return CanonicalItemPredicates.inPool((DataHubPoolEntity) pool).and(CanonicalItemPredicates.hasType(type))
				.and(QManagedCanonicalItemEntity.managedCanonicalItemEntity.integrationKey.startsWith(intKey)).and(CanonicalItemPredicates.hasStatuses(statuses));
	}

	private <T extends CanonicalItem> List<T> convertQueryResults(Iterable<ManagedCanonicalItemEntity> persistedItems) {
		ArrayList<ManagedCanonicalItemEntity> managedItems = Lists.newArrayList(persistedItems);
		ArrayList<T> foundItems = new ArrayList<T>();
		Iterator<ManagedCanonicalItemEntity> arg3 = managedItems.iterator();

		while (arg3.hasNext()) {
			foundItems.add(this.converter.toCanonicalItem(arg3.next()));
		}

		return foundItems;
	}

	@Required
	public void setRepository(YJpaRepository<ManagedCanonicalItemEntity> repo) {
		this.jpaRepository = repo;
		super.setRepository(repo);
	}

	@Required
	public void setItemConverter(CanonicalItemConverter c) {
		this.converter = c;
		super.setItemConverter(c);
	}

}
