package za.co.massmart.datahub.service;

import java.util.Collection;

import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.runtime.domain.DataHubPool;

public interface CanonicalItemService extends com.hybris.datahub.service.CanonicalItemService {

	Collection<CanonicalItem> findItemsByPartOfIntegrationKey(String typeCode, String key,
			DataHubPool pool);

}