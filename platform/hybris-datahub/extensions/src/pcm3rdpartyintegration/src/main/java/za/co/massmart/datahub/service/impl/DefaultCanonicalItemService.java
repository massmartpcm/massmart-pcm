package za.co.massmart.datahub.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.hybris.datahub.domain.CompositionStatusType;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.runtime.domain.DataHubPool;
import za.co.massmart.datahub.repository.CanonicalItemRepository;
import za.co.massmart.datahub.service.CanonicalItemService;

public class DefaultCanonicalItemService extends com.hybris.datahub.service.impl.DefaultCanonicalItemService implements CanonicalItemService {

	private CanonicalItemRepository canonicalItemRepository;

	public Collection<CanonicalItem> findItemsByPartOfIntegrationKey(String typeCode, String key,
			DataHubPool pool) {
		Preconditions.checkArgument(typeCode != null, "typeCode cannot be null");
		Preconditions.checkArgument(key != null, "integrationKey cannot be null");
		Preconditions.checkArgument(pool != null, "pool cannot be null");
		Preconditions.checkArgument(CanonicalItem.isValidType(typeCode), typeCode + " is not a valid canonical type");
		return this.canonicalItemRepository.findByPoolAndPartOfIntegrationKey(pool, key, typeCode,
				CompositionStatusType.SUCCESS);
	}

	@Required
	public void setCanonicalItemRepository(CanonicalItemRepository canonicalItemRepository) {
		this.canonicalItemRepository = canonicalItemRepository;
	}

}
