package za.co.massmart.datahub.repository;

import java.util.List;

import com.hybris.datahub.domain.CompositionStatusType;
import com.hybris.datahub.model.CanonicalItem;
import com.hybris.datahub.runtime.domain.DataHubPool;

public interface CanonicalItemRepository extends com.hybris.datahub.repository.CanonicalItemRepository {

	List<CanonicalItem> findByPoolAndPartOfIntegrationKey(DataHubPool arg0, String arg1, String arg2,
			CompositionStatusType... arg3);

}