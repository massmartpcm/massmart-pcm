package za.co.massmart.datahub.service;


import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.Resource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.hybris.datahub.domain.AttributeDefinition;
import com.hybris.datahub.dto.integration.RawFragmentData;
import com.hybris.datahub.service.RawItemMetadataService;


class XMLDocDefaultHandler extends DefaultHandler
{
	private final Set<String> attributeNames;
	private final Set<String> attributeSegments;
	private final StringBuilder currentSegment = new StringBuilder(30);
	private String excludedSegment;
	private boolean excludeSegment;
	private final SegmentTree head = new SegmentTree(null, null);
	private boolean inIDOC = false;
	private String lastString = null;
	private final String[] segmentStack = new String[30];
	private int segmentStackIndex = -1;
	private SegmentTree tree = head;
	private String rootElement;

	public XMLDocDefaultHandler(Set<String> attributeNames, Set<String> attributeSegments, String rootElement)
	{
		this.attributeNames = attributeNames;
		this.attributeSegments = attributeSegments;
		this.rootElement = rootElement;
	}

	@Override
	public void characters(final char[] ch, final int start, final int length) throws SAXException
	{
		if (excludeSegment || !inIDOC)
		{
			return;
		}
//		String value = new String(ch, start, length).trim();
//		if (value.length() == 0)
//        {
//            return; // ignore white space
//        }
//		final String newString = new String(ch, start, length);
//		lastString = lastString != null ? lastString.concat(newString) : newString;
		
		for (int i = start, n = start + length; i < n; i++)
		{
			if (ch[i] > ' ')
			{
				final String newString = new String(ch, start, length);
				lastString = lastString != null ? lastString.concat(newString) : newString;
				return;
			}
		}
	}

	@Override
	public void endElement(final String uri, final String localName, final String qName) throws SAXException
	{
		if (excludeSegment)
		{
			if (excludedSegment.equals(qName))
			{
				excludeSegment = false;
			}
			return;
		}

		if (!inIDOC)
		{
			return;
		}

		if (rootElement.equals(qName))
		{
			inIDOC = false;
			tree = tree.previous;
			return;
		}

		if (lastString != null)
		{
			segmentStackIndex--; 
			tree = tree.previous;
			currentSegment.setLength(currentSegment.length() - (qName.length() + 1));
			final int oldLength = currentSegment.length();
			currentSegment.append(qName);
			final String attribute = currentSegment.toString();
			currentSegment.setLength(oldLength);
			if (this.attributeNames.contains(attribute))
			{
				tree.map.put(attribute, lastString);
			}
			lastString = null;
		}
		else if (segmentStack[segmentStackIndex].equals(qName))
		{
			segmentStackIndex--;
			final int oldLength = currentSegment.length() - 1;
			currentSegment.setLength(oldLength - qName.length());
			tree = tree.previous;
		}
	}

	public List<List<Map<String, String>>> getMaps()
	{
		return head.getMapsList();
	}

	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes attributes)
			throws SAXException
	{
		if (excludeSegment)
		{
			return;
		}

		if (!inIDOC)
		{
			if (rootElement.equals(qName))
			{
				inIDOC = true;
				tree = new SegmentTree(tree, "");
			}
			return;
		}

		if (!this.attributeSegments.contains(qName))
		{
			excludedSegment = qName;
			excludeSegment = true;
			return;
		}

//		if (attributes.getLength() != 0)
//		{
			currentSegment.append(qName);
			currentSegment.append('-');
			segmentStack[++segmentStackIndex] = qName;
			tree = new SegmentTree(tree, qName);
//		}
	}
}

/** */
public class XMLDocMappingService
{
	private static final Comparator<Entry<String, ?>> ENTRY_ORDER_DASH = Comparator.comparingInt(e -> e.getKey().chars()
			.filter(c -> c == '-').map(c -> 1).sum());
	private static final Comparator<Entry<String, ?>> ENTRY_ORDER_KEY = Comparator.comparing(Entry::getKey);
	protected static final Comparator<Entry<String, ?>> ENTRY_ORDER_Z = ENTRY_ORDER_DASH.thenComparing(ENTRY_ORDER_KEY);

	private static final Logger LOGGER = LoggerFactory.getLogger(XMLDocMappingService.class);

	private static final ThreadLocal<SAXParser> PARSERS = ThreadLocal.withInitial(() -> {
		try
		{
			return SAXParserFactory.newInstance().newSAXParser();
		}
		catch (ParserConfigurationException | SAXException e)
		{
			throw new RuntimeException(e.getMessage(), e);
		}
	});

	private Set<String> attributeNames;
	private Set<String> attributeSegments;
	protected String rawFragmentDataExtensionSource = "sapidocintegration";
	protected String rawFragmentDataFeedName = "DEFAULT_FEED";
	protected String rawFragmentDataType;
	protected String rawFragmentRootElement;
	protected RawItemMetadataService rawItemMetadataService;


	/**
	 * Extends this method for custom code.<br>
	 * Add or remove maps.<br>
	 * Add or remove values.<br>
	 * 
	 * @param maps
	 *           {@link List} of {@link Map} representing a csv line
	 */
	protected void customizeMaps(final List<Map<String, String>> maps)
	{
		this.print(maps);
	}

	protected Set<String> getAttributeNames()
	{
		if (this.attributeNames == null)
		{
			this.attributeNames = this.rawItemMetadataService.findAttributeDefinitionsByRawType(rawFragmentDataType).stream() //
					.map(AttributeDefinition::getAttributeName) //
					.map(String::intern) //
					.collect(Collectors.toCollection(() -> new HashSet<>(64, 0.20f)));
			LOGGER.debug("rawFragmentDataType={}, attributeNames={}", this.rawFragmentDataType, this.attributeNames);
		}
		return this.attributeNames;
	}

	protected Set<String> getAttributeSegments()
	{
		if (this.attributeSegments == null)
		{
			// Split and rebuild all possible ordered permutation of the '-' within all Strings.
			this.attributeSegments = this.getAttributeNames().stream() //
					.map(s -> s.split("-")) //
					.map(array -> IntStream.range(0, array.length) //
							.mapToObj(i -> IntStream.rangeClosed(0, array.length - i) //
									.<String> mapToObj(j -> Arrays.stream(array) //
											.skip(i) //
											.limit(j) //
											.collect(Collectors.joining("-")))) //
							.flatMap(Function.identity())) //
					.flatMap(Function.identity()) //
					.filter(s -> !s.isEmpty()) //
					.map(String::intern) //
					.collect(Collectors.toCollection(() -> new HashSet<>(256, 0.20f)));
			LOGGER.debug("rawFragmentDataType={}, attributeSegments={}", this.rawFragmentDataType, this.attributeSegments);
		}
		return this.attributeSegments;
	}

	/**
	 * @param xml
	 *           IDOC XML
	 * @return {@link List} of {@link RawFragmentData}
	 */
	public final List<RawFragmentData> map(final String xml)
	{
		final long start = System.currentTimeMillis();

		final XMLDocDefaultHandler dh = new XMLDocDefaultHandler(this.getAttributeNames(), this.getAttributeSegments(), this.rawFragmentRootElement);
		try
		{
			final SAXParser saxParser = PARSERS.get(); // Thread scope
			saxParser.parse(new InputSource(new StringReader(xml)), dh);
			saxParser.reset(); // Release the IDOCDefaultHandler's SegmentTree.
		}
		catch (final SAXException | IOException e)
		{
			throw new RuntimeException(this + " : Exception parsing : " + xml, e);
		}
		final List<List<Map<String, String>>> mapsList = dh.getMaps();
		final List<RawFragmentData> data = new ArrayList<>();
		int count = 0;
		for (int i = 0, n = mapsList.size(); i < n; i++)
		{
			final List<Map<String, String>> maps = mapsList.get(i);
			this.customizeMaps(maps);
			this.reduceMaps(maps);
			for (int j = 0, m = maps.size(); j < m; j++)
			{
				final Map<String, String> map = maps.get(j);
				count += map.size();
				data.add(new RawFragmentData(rawFragmentDataType, map, rawFragmentDataFeedName, rawFragmentDataExtensionSource));
			}
		}
		LOGGER.debug("The parsing of the IDOC took {} milliseconds. {} IDOCs, {} fragments, {} values.",
				(System.currentTimeMillis() - start), mapsList.size(), data.size(), count);
		return data;
	}

	/**
	 * Friendly print of the list of maps
	 * 
	 * @param maps
	 *           {@link List} of {@link Map} representing a csv line
	 */
	private final void print(final List<Map<String, String>> maps)
	{
		if (LOGGER.isTraceEnabled())
		{
			for (final Map<String, String> map : maps)
			{
				LOGGER.trace("{}", map.entrySet().stream().sorted(ENTRY_ORDER_Z).collect(Collectors.toList()));
			}
		}
	}

	private void reduceMaps(List<Map<String, String>> maps)
	{
		if (maps.size() < 2)
		{
			return;
		}

		Iterator<Map<String, String>> iterator = maps.iterator();
		Map<String, String> lastMap = iterator.next();
		int lastHashCode = lastMap.hashCode();
		while (iterator.hasNext())
		{
			Map<String, String> nextMap = iterator.next();
			int nextHashCode = nextMap.hashCode();
			if (lastHashCode == nextHashCode && lastMap.equals(nextMap))
			{
				iterator.remove();
			}
			else
			{
				lastMap = nextMap;
				lastHashCode = nextHashCode;
			}
		}
	}

	/**
	 * @param rawFragmentDataExtensionSource
	 *           Raw Fragment Data Extension Source
	 * @see RawFragmentData#setExtensionSource(String)
	 */
	public void setRawFragmentDataExtensionSource(final String rawFragmentDataExtensionSource)
	{
		LOGGER.debug("rawFragmentDataExtensionSource={}", rawFragmentDataExtensionSource);
		this.rawFragmentDataExtensionSource = rawFragmentDataExtensionSource.intern();
	}

	/**
	 * @param rawFragmentDataFeedName
	 *           Raw Fragment Data Feed Name
	 * @see RawFragmentData#setDataFeedName(String)
	 */
	public void setRawFragmentDataFeedName(final String rawFragmentDataFeedName)
	{
		LOGGER.debug("rawFragmentDataFeedName={}", rawFragmentDataFeedName);
		this.rawFragmentDataFeedName = rawFragmentDataFeedName.intern();
	}

	/**
	 * @param rawFragmentDataType
	 *           Raw Fragment Data Type
	 * @see RawFragmentData#setType(String)
	 */
	@Required
	public void setRawFragmentDataType(final String rawFragmentDataType)
	{
		LOGGER.debug("rawFragmentDataType={}", rawFragmentDataType);
		this.rawFragmentDataType = rawFragmentDataType.intern();
	}

	@Required
	public void setRawFragmentRootElement(final String rawFragmentRootElement)
	{
		LOGGER.debug("rawFragmentRootElement={}", rawFragmentRootElement);
		this.rawFragmentRootElement = rawFragmentRootElement.intern();
	}

	// Autowire Data Hub bean
	@Resource(name = "rawItemMetadataService")
	public void setRawItemMetadataService(final RawItemMetadataService rawItemMetadataService)
	{
		this.rawItemMetadataService = rawItemMetadataService;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("IDOCMappingService [rawFragmentDataExtensionSource=");
		builder.append(rawFragmentDataExtensionSource);
		builder.append(", rawFragmentDataFeedName=");
		builder.append(rawFragmentDataFeedName);
		builder.append(", rawFragmentDataType=");
		builder.append(rawFragmentDataType);
		builder.append("]");
		return builder.toString();
	}
}

class SegmentTree
{
	private static final Integer INTEGER_ZERO = Integer.valueOf(0);
	private final List<SegmentTree> list = new ArrayList<>();
	final Map<String, String> map = new HashMap<>();
	final SegmentTree previous;
	private final String segment;

	SegmentTree(final SegmentTree previous, final String segment)
	{
		this.previous = previous;
		this.segment = segment;
		if (previous != null)
		{
			previous.list.add(this);
		}
	}

	private List<Map<String, String>> getMaps()
	{
		Map<String, Integer> indexes = null;
		Map<String, Map<String, String>> singleMaps = null;
		List<Map<String, String>> maps = null;
		for (int i = 0, n = list.size(); i < n; i++)
		{
			if (maps == null)
			{
				maps = new ArrayList<>();
			}
			if (indexes == null)
			{
				indexes = new HashMap<>();
			}
			if (singleMaps == null)
			{
				singleMaps = new HashMap<>();
			}
			final SegmentTree st = list.get(i);
			final List<Map<String, String>> listMaps = st.getMaps();
			final int listMapsSize = listMaps.size();

			Integer index = indexes.get(st.segment);
			index = index != null ? index : INTEGER_ZERO;
			if (listMapsSize == 1 && index == 0)
			{
				singleMaps.put(st.segment, new HashMap<>(listMaps.get(0)));
			}
			for (int j = 0; j < listMapsSize; j++)
			{
				if (index + j < maps.size())
				{
					maps.get(index + j).putAll(listMaps.get(j));
				}
				else
				{
					maps.add(listMaps.get(j));
				}
			}
			indexes.put(st.segment, index + listMapsSize);
		}
		if (maps == null)
		{
			return Collections.singletonList(map);
		}
		for (Entry<String, Integer> e : indexes.entrySet())
		{
			if (e.getValue().intValue() == 1)
			{
				map.putAll(singleMaps.get(e.getKey()));
			}
		}
		for (int i = 0, n = maps.size(); i < n; i++)
		{
			maps.get(i).putAll(map);
		}
		return maps;
	}

	List<List<Map<String, String>>> getMapsList()
	{
		return list.stream().map(SegmentTree::getMaps).collect(Collectors.toList());
	}
}
