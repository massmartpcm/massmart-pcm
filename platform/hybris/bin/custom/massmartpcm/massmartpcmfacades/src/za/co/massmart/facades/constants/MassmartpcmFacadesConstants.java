/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package za.co.massmart.facades.constants;

/**
 * Global class for all MassmartpcmFacades constants.
 */
@SuppressWarnings("PMD")
public class MassmartpcmFacadesConstants extends GeneratedMassmartpcmFacadesConstants
{
	public static final String EXTENSIONNAME = "massmartpcmfacades";

	private MassmartpcmFacadesConstants()
	{
		//empty
	}
}
