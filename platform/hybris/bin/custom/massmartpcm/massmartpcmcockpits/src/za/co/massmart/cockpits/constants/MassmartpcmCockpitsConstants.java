/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package za.co.massmart.cockpits.constants;

/**
 * Global class for all MassmartpcmCockpits constants. You can add global constants for your extension into this class.
 */
public final class MassmartpcmCockpitsConstants extends GeneratedMassmartpcmCockpitsConstants
{
	public static final String EXTENSIONNAME = "massmartpcmcockpits";

	public static final String JASPER_REPORTS_MEDIA_FOLDER = "jasperreports";

	private MassmartpcmCockpitsConstants()
	{
		// empty
	}
}
