package za.co.massmart.core.jalo.productfeatures;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;

import org.apache.log4j.Logger;

import za.co.massmart.core.model.catalog.DependantProductFeatureModel;

import de.hybris.platform.catalog.jalo.ProductFeature;

/**
 * Translator which makes all cost centers inactive.
 *
 * @author chriss
 *
 */
public class ProductFeatureCleanupTranslator extends AbstractSpecialValueTranslator
{
    private static final Logger LOG = Logger.getLogger(ProductFeatureCleanupTranslator.class);

    private ModelService modelService;

    @Override
    public void performImport(final String cellValue, final Item processedItem)
    {

        if (processedItem != null)
        {
            final ProductModel productModel = getModelService().get(processedItem.getPK());
            LOG.info("Product = " + productModel.getCode());
            final Collection<ProductFeatureModel> productFeatureModelList = productModel.getFeatures();
            if (productFeatureModelList != null && !productFeatureModelList.isEmpty())
            {
                for (final ProductFeatureModel productFeatureModel : productFeatureModelList)
                {
                    if (!productFeatureModel.getClassificationAttributeAssignment().getClassificationClass().getCode().equals(cellValue)) {
                        continue;
                    }
                    LOG.info("ProductFeatureModel " + productFeatureModel.getQualifier());
                    final Collection<DependantProductFeatureModel> dependantProductFeatureModelList = productFeatureModel
                            .getDependantProductFeatures();
                    if (dependantProductFeatureModelList != null && !dependantProductFeatureModelList.isEmpty())
                    {
                        for (final DependantProductFeatureModel dependantProductFeatureModel : dependantProductFeatureModelList)
                        {
                            modelService.remove(dependantProductFeatureModel);
                        }
                    }
                    modelService.remove(productFeatureModel);
                }
            }
        }
    }

    @Override
    public String performExport(final Item processedItem)
    {
        return "";
    }


    protected ModelService getModelService()
    {
        if (this.modelService == null)
        {
            this.modelService = (Registry.getApplicationContext().getBean("modelService", ModelService.class));
        }

        return this.modelService;
    }
}