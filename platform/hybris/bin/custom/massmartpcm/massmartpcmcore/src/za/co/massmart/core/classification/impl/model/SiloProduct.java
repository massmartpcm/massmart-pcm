package za.co.massmart.core.classification.impl.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SiloProduct {

    private static final String PROPERTY_BASE_INFO = "base_info";
    private static final String PROPERTY_TITLE = "title";
    private static final String PROPERTY_BARCODE = "unit_barcode";
    private static final String PROPERTY_DESCRIPTION = "description";
    private static final String PROPERTY_IMAGES = "images";

    private static final String PRIMARY_VIEW_NAME = "front";

    private JsonObject product;
    private String barcode;

    public SiloProduct(JsonObject product) {
        this(null, product);
    }

    public SiloProduct(String barcode, JsonObject product) {
        this.barcode = barcode;
        this.product = product;
    }

    public String getName() {
        return product.getAsJsonObject(PROPERTY_BASE_INFO).get(PROPERTY_TITLE).getAsString();
    }

    public JsonObject getJsonObject() {
        return product;
    }

    public String getDescription() {
        JsonObject baseInfo = product.getAsJsonObject(PROPERTY_BASE_INFO);
        JsonElement description = baseInfo == null ? null : baseInfo.get(PROPERTY_DESCRIPTION);
        return description == null || description.isJsonNull() ? null : description.getAsString();
    }

    public String getBarcode() {
        if (barcode != null) {
            return barcode;
        }
        JsonObject baseInfo = product.getAsJsonObject(PROPERTY_BASE_INFO);
        JsonElement barcode = baseInfo == null ? null : baseInfo.get(PROPERTY_BARCODE);
        return barcode == null || barcode.isJsonNull() ? null : barcode.getAsString();
    }

    /**
     * Parses the JSON represented by this product to determine the images it repreesnts.
     *
     * This method takes into account two possible structures for Silo images in JSON format.
     *
     * <ol>
     *     <li>As an object with arbitrary numbers as the members:
     *     <pre>
     *   "images": {
     *     "front": {
     *       "17": {
     *         "uid": "123167",
     *         "large": "http:\/\/library.silo.co.za\/img\/products\/20140827\/143691\/p450_53fd878d4d57b.jpg",
     *         ...
     *       },
     *       "18": {
     *         "uid": "123172",
     *         "large": "http:\/\/library.silo.co.za\/img\/products\/20140827\/143691\/p450_53fd8929200ca.jpg",
     *         ..
     *       }
     *     }
     *   }
     *   </pre>
     *   </li>
     *   <li>OR as an array:
     *   <pre>
     *   "images": {
     *     "front": [
     *       {
     *         "uid": "43572",
     *         "large": "http:\/\/library.silo.co.za\/img\/products\/20140101\/9979\/p450_pr_6006040000745_20130804191234.jpg",
     *         ...
     *       },
     *       {
     *         "uid": "151976",
     *         "large": "http:\/\/library.silo.co.za\/img\/products\/20140101\/9979\/p450_54ffe091c172f.jpg",
     *         ...
     *       }
     *     ]
     *   }
     *   </pre>
     *   </li>
     * </ol>
     *
     * @return the resulting parsed set of images
     */
    public List<SiloImage> getSiloImages() {
        JsonObject images = product.getAsJsonObject(PROPERTY_IMAGES);
        List<SiloImage> list = new ArrayList<>();
        /*The following complicated-looking arrangement deals with the two possible structures for Silo
        images, as explained in the JavaDoc comment above.*/
        for (Map.Entry<String, JsonElement> image : images.entrySet()) {
            JsonElement instancesOfImageView = image.getValue();
            if (instancesOfImageView.isJsonArray()) {
                for (JsonElement instanceOfImageView : instancesOfImageView.getAsJsonArray()) {
                    list.add(new SiloImage(image.getKey(), instanceOfImageView.getAsJsonObject()));
                }
            } else if (instancesOfImageView.isJsonObject()) {
                for (Map.Entry<String, JsonElement> instanceOfImageView : instancesOfImageView.getAsJsonObject().entrySet()) {
                    list.add(new SiloImage(image.getKey(), instanceOfImageView.getValue().getAsJsonObject()));
                }
            } else {
                throw new RuntimeException("Malformed image data: expected image view to be JSON array or object; got " + instancesOfImageView);
            }
        }
        return list;
    }

    /**
     * A list of unique Silo images per view.
     *
     * @return
     */
    public List<SiloImage> getUniqueSiloImages() {

        // We'll use this list later to pull items out of the map in the correct order
        List<String> viewOrder = new ArrayList<String>();

        // The map will contain the latest siloImage per view
        Map<String, SiloImage> map = new HashMap<>();

        for (SiloImage siloImage: getSiloImages()) {
            String view = siloImage.getView();
            if (!viewOrder.contains(view)) {
                viewOrder.add(view);
            }
            chooseImageByGreatestId(map, siloImage, view);
            // chooseImageByLatestCreatedDate(map, siloImage, view);
        }

        // Create the result list by pulling items from the map in the correct order
        List<SiloImage> result = new ArrayList<>(map.size());
        for (String view: viewOrder) {
            SiloImage image = map.get(view);
            image.setPrimary(PRIMARY_VIEW_NAME.equals(view));
            result.add(image);
        }

        return result;
    }

    /**
     * If silo supplied two images for the "front" view of a product, choose the image with the largest uid.
     *
     * @param map
     * @param siloImage
     * @param view
     */
    protected void chooseImageByGreatestId(Map<String, SiloImage> map, SiloImage siloImage, String view) {
        Integer otherValue = 0;
        if (map.containsKey(view)) {
            otherValue = toInt(map.get(view).getId());
        }
        Integer thisValue = toInt(siloImage.getId());
        // If thisValue >= otherValue...
        if (!(thisValue < otherValue)) {
            map.put(view, siloImage);
        }
    }

    /**
     * We just know they'll have some dodgy data..
     *
     * @param string
     * @return
     */
    private Integer toInt(String string) {
        try {
            return Integer.valueOf(string);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * If silo supplied two images for the "front" view of a product, choose the image with the latest created date.
     *
     * @param map
     * @param siloImage
     * @param view
     */
    protected void chooseImageByLatestCreatedDate(Map<String, SiloImage> map, SiloImage siloImage, String view) {
        // The formatter is cached so it is not expensive to instantiate this here
        DateTime otherValue = new DateTime(0);
        if (map.containsKey(view)) {
            otherValue = toDateTime(map.get(view).getWhenCreated());
        }
        DateTime thisValue = toDateTime(siloImage.getWhenCreated());
        // If thisValue is greater or equal to...
        if (!thisValue.isBefore(otherValue)) {
            map.put(view, siloImage);
        }
    }

    /**
     * Silo use the date value "0000-00-00 00:00:00" which is not a valid date
     *
     * @param string
     * @return
     */
    private DateTime toDateTime(String string) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        try {
            return formatter.parseDateTime(string);
        } catch (Exception e) {
            // Silo use the date value "0000-00-00 00:00:00" which is not a valid date
            return new DateTime(0);
        }
    }

}