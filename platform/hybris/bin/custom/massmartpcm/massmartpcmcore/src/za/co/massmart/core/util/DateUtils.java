package za.co.massmart.core.util;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Util class for Date operations
 */
public class DateUtils {

    private static final Logger LOG = Logger.getLogger(DateUtils.class.getName());

    public static String formatDate(final Date dateToFormat, final String formatPattern) {
        final SimpleDateFormat dt = new SimpleDateFormat(formatPattern);
        return dt.format(dateToFormat);
    }

    public static LocalDate convertDateToLocalDate(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
                .toLocalDate();
    }

    public static Date convertStringToDate(String formatDate) {
        Date date = null;

        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(formatDate);
        } catch (ParseException e) {
            LOG.error("Failed to parse date " + formatDate);
        }
        return date;
    }

    public static Date getZeroTimeDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        return date;
    }

    public static Date getMaxDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(9999, 11, 31);
        Date date = new Date(cal.getTimeInMillis());
        return date;
    }

}
