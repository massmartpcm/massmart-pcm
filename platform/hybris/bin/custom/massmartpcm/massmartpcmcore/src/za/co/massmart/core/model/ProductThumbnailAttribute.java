package za.co.massmart.core.model;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.apache.commons.collections.CollectionUtils;

public class ProductThumbnailAttribute extends AbstractDynamicAttributeHandler<MediaModel, ProductModel>
{
    private MediaContainerService mediaContainerService;
    private MediaService mediaService;

    @Override
    public MediaModel get(final ProductModel product)
    {
        if (CollectionUtils.isNotEmpty(product.getGalleryImages()))
        {
            //Retrieve the first image for the thumbnail
            final MediaContainerModel mediaContainer = product.getGalleryImages().get(0);
            final MediaFormatModel mediaFormat = getMediaService().getFormat("thumbnail");
            try
            {
                final MediaModel media = getMediaContainerService().getMediaForFormat(mediaContainer, mediaFormat);

                if (media != null)
                {
                    return media;
                }

            }
            catch (final ModelNotFoundException e)
            {
                return null;
            }
        }
        return null;
    }

    @Override
    public void set(final ProductModel arg0, final MediaModel arg1)
    {
        // do nothing
    }

    /**
     * @return the mediaContainerService
     */
    public MediaContainerService getMediaContainerService()
    {
        return mediaContainerService;
    }

    /**
     * @param mediaContainerService
     *           the mediaContainerService to set
     */
    public void setMediaContainerService(final MediaContainerService mediaContainerService)
    {
        this.mediaContainerService = mediaContainerService;
    }

    /**
     * @return the mediaService
     */
    public MediaService getMediaService()
    {
        return mediaService;
    }

    /**
     * @param mediaService
     *           the mediaService to set
     */
    public void setMediaService(final MediaService mediaService)
    {
        this.mediaService = mediaService;
    }

}
