package za.co.massmart.core.classification.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import za.co.massmart.core.classification.impl.model.SiloProduct;
import za.co.massmart.core.classification.service.ProductClassificationImportService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SiloProductClassificationImportService implements ProductClassificationImportService {

    private static final Logger log = LoggerFactory.getLogger(SiloProductClassificationImportService.class);

    private ModelService modelService;
    private SiloProductProcessor siloProductProcessor;

    @Override
    public void importClassificationsForSingleProduct(String ean, CatalogVersionModel catalogVersion, String clientId,
                                                      String clientAuthentication, String externalServiceSingleProductEndpoint,
                                                      ClassificationClassModel rootClassificationClass) {
        SiloClient siloClient = new SiloClient(externalServiceSingleProductEndpoint);
        SiloProduct product = siloClient.getProduct(clientId, clientAuthentication, ean);
        if (product == null) {
            throw new UnknownIdentifierException("No classifications found for product " + ean);
        }
        siloProductProcessor.processSiloProductAndAddClassifications(catalogVersion, product, rootClassificationClass);
    }

    @Override
    public boolean importClassifications(CatalogVersionModel catalogVersionModel, String clientId, String clientAuthentication,
                                         DateTime startDate, DateTime endDate, DateTimeZone externalServiceTimeZone, String externalServiceEndpoint,
                                         String externalServicePageAddition, int numberOfDownloadThreads,
                                         ClassificationClassModel rootClassificationClass, boolean replaceExistingClassifications) {

        log.info("Silo import service importing products from " + startDate + " to " + endDate);

        SiloClient siloClient = new SiloClient(externalServiceTimeZone, externalServiceEndpoint, externalServicePageAddition);

        List<SiloProduct> productList = siloClient.getProductList(clientId, clientAuthentication, startDate, endDate, numberOfDownloadThreads);

        if (replaceExistingClassifications) {
            log.info("Deleting existing classifications");
            deleteChildClassifications(rootClassificationClass, false);
        }

        // Choose the new value in the list - this presupposes that the data received from Silo is in date order...which it possibly isn't
        Map<String, SiloProduct> siloProductsMap = productList.stream().collect(Collectors.toMap(SiloProduct::getBarcode, item -> item, (oldValue, newValue) -> newValue));

        log.info("Silo import service found " + siloProductsMap.size() + " unique products");

        int skipped = 0, count = 0, errors = 0;

        for (SiloProduct siloProduct : siloProductsMap.values()) {
            try {
                ProductModel product = siloProductProcessor.processSiloProductAndAddClassifications(catalogVersionModel, siloProduct, rootClassificationClass);
                if (product == null) {
                    skipped++;
                }
            } catch (Exception e) {
                errors++;
                try {
                    log.error("Failed to process Silo product " + siloProduct.getBarcode() + ": " + e.getMessage(), e);
                } catch (Exception e2) {
                    log.error("Failed to process Silo product -- could not determine barcode. " + e.getMessage() + "; " + siloProduct.getJsonObject(), e2);
                }
            }
            finally {
                if ((++count % 100) == 0) {
                    log.info("Processed {} of {} (skipped {}; errors {}).", count, siloProductsMap.size(), skipped, errors);
                }
            }
        }

        log.info("Silo import service finished. Updated: {}, Skipped: {}, Errors: {}, Total: {}", siloProductsMap.size() - skipped - errors, skipped, errors, siloProductsMap.size());

        return errors == 0;
    }

    protected void deleteChildClassifications(ClassificationClassModel classification, boolean deleteCurrentClassification) {
        for (CategoryModel childCategory : classification.getCategories()) {
            if (childCategory instanceof ClassificationClassModel) {
                deleteChildClassifications((ClassificationClassModel) childCategory, true);
            }
        }

        modelService.refresh(classification);

        if (deleteCurrentClassification) {
            List<ClassificationAttributeModel> declaredClassificationAttributes = classification.getDeclaredClassificationAttributes();
            modelService.removeAll(classification.getDeclaredClassificationAttributeAssignments());
            modelService.removeAll(declaredClassificationAttributes);
            modelService.remove(classification);
        }
    }

    @Required
    public void setSiloProductProcessor(SiloProductProcessor siloProductProcessor) {
        this.siloProductProcessor = siloProductProcessor;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

}
