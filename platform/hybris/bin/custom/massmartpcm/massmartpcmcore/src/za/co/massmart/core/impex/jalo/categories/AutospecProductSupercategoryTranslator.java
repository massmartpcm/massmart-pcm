package za.co.massmart.core.impex.jalo.categories;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;


/**
 * SupercategoryTranslator which ensures that only Autospec categories are updated. builderProductCatalog categories are
 * retained.
 *
 * @author garyb
 *
 */
public class AutospecProductSupercategoryTranslator extends AbstractValueTranslator
{
    private static final Logger LOG = Logger.getLogger(AutospecProductSupercategoryTranslator.class);

    private CategoryService categoryService;
    private ModelService modelService;
    private CatalogVersionService catalogVersionService;

    private final String catalogName = Config.getString("autospec.classification.catalog", "AUTOSPEC_CLASSIFICATION_CATALOG");
    private final String catalogVersionName = Config.getString("autospec.classification.catalog.version", "AS_IMPORT");

    @Override
    public Object importValue(final String cellValue, final Item processedItem)
    {
        final String[] cellValueArray = cellValue != null ? cellValue.split(",") : null;
        LOG.info("cell value array contains : " + cellValueArray.length);

        final Collection<CategoryModel> categoryModelList = new ArrayList<CategoryModel>();

        final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(catalogName, catalogVersionName);

        if (processedItem != null)
        {
            final ProductModel productModel = getModelService().get(processedItem.getPK());
            LOG.info("Product = " + productModel.getCode());
            final Collection<CategoryModel> productCategoryModelList = productModel.getSupercategories();
            if (productCategoryModelList != null && !productCategoryModelList.isEmpty())
            {
                for (final CategoryModel category : productCategoryModelList)
                {
                    LOG.info("Product Category " + category.getCode());
                    if (!category.getCatalogVersion().equals(catalogVersion))
                    {
                        categoryModelList.add(category);
                    }
                }
            }
        }
        for (final String categoryCode : cellValueArray)
        {
            final String code = categoryCode.split(":")[0];
            LOG.info("New Category = " + code);
            final CategoryModel newCategoryModel = getCategoryService().getCategoryForCode(catalogVersion, code);
            categoryModelList.add(newCategoryModel);
        }

        return categoryModelList;
    }

    @Override
    public String exportValue(final Object value)
    {
        return "";
    }

    protected CategoryService getCategoryService()
    {
        if (this.categoryService == null)
        {
            this.categoryService = (Registry.getApplicationContext().getBean("categoryService", CategoryService.class));
        }

        return this.categoryService;
    }

    protected CatalogVersionService getCatalogVersionService()
    {
        if (this.catalogVersionService == null)
        {
            this.catalogVersionService = (Registry.getApplicationContext().getBean("catalogVersionService",
                    CatalogVersionService.class));
        }

        return this.catalogVersionService;
    }

    protected ModelService getModelService()
    {
        if (this.modelService == null)
        {
            this.modelService = (Registry.getApplicationContext().getBean("modelService", ModelService.class));
        }

        return this.modelService;
    }
}