package za.co.massmart.core.listeners;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import za.co.massmart.core.enums.ClassificationSystemVisibilityEnum;
import za.co.massmart.core.model.catalog.DependantProductFeatureModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductFeatureAfterSaveListener implements AfterSaveListener {

    private static final Logger LOG = Logger.getLogger(ProductFeatureAfterSaveListener.class);

    //The product feature deployment code is "611"
    private static final int PRODUCT_FEATURE_DEPLOYMENT = 611;

    private ModelService modelService;

    @Override
    public void afterSave(Collection<AfterSaveEvent> events) {
        for (final AfterSaveEvent event : events) {
            final int type = event.getType();
            if (AfterSaveEvent.UPDATE == type || AfterSaveEvent.CREATE == type) {
                final PK pk = event.getPk();

                if (PRODUCT_FEATURE_DEPLOYMENT == pk.getTypeCode()) {

                    performFeatureSync(pk);

                }
            }
        }
    }

    private void performFeatureSync(PK pk) {
        final ProductFeatureModel productFeatureModel = modelService.get(pk);

        ProductModel product = productFeatureModel.getProduct();
        List<ProductFeatureModel> features = product.getFeatures();

        String sourceName = productFeatureModel.getClassificationAttributeAssignment().getClassificationAttribute().getName();
        ClassificationSystemModel sourceClassificationCatalog = productFeatureModel.getClassificationAttributeAssignment().getClassificationClass().getCatalogVersion().getCatalog();
        ClassificationSystemVersionModel sourceClassificationCatalogVersion = productFeatureModel.getClassificationAttributeAssignment().getClassificationClass().getCatalogVersion();

        ClassAttributeAssignmentModel classificationAttributeAssignment;
        if ((classificationAttributeAssignment = productFeatureModel.getClassificationAttributeAssignment()) != null) {
            ClassAttributeAssignmentModel target;
            if ((target = classificationAttributeAssignment.getSyncTarget()) != null) {

                ClassificationSystemModel targetClassificationCatalog = target.getClassificationClass().getCatalogVersion().getCatalog();
                ClassificationSystemVersionModel targetClassificationCatalogVersion = target.getClassificationClass().getCatalogVersion();
                String targetName = target.getClassificationAttribute().getName();

                LOG.info(String.format("Product feature %s:%s [%s] is mapped to %s:%s [%s]", sourceClassificationCatalog.getId(), sourceClassificationCatalogVersion.getVersion(), sourceName, targetClassificationCatalog.getId(), targetClassificationCatalogVersion.getVersion(), targetName));

                if (checkSyncConditions(sourceClassificationCatalog, targetClassificationCatalog, productFeatureModel, target)) {

                    syncProductFeature(features, target, productFeatureModel, targetClassificationCatalog, targetClassificationCatalogVersion, product);

                }
            }
        }

    }

    private void syncProductFeature(List<ProductFeatureModel> features, ClassAttributeAssignmentModel target, ProductFeatureModel productFeatureModel, ClassificationSystemModel targetClassificationCatalog, ClassificationSystemVersionModel targetClassificationCatalogVersion, ProductModel product) {
        //TODO support for multivalue features

        for (ProductFeatureModel feature : features) {
            if (feature.getClassificationAttributeAssignment().equals(target)) {
                LOG.info("target product feature exists, cloning");

                modelService.removeAll(feature.getDependantProductFeatures()); //<------ make sure there are no orphans
                copyFeature(productFeatureModel, feature, target, targetClassificationCatalog, targetClassificationCatalogVersion);

                modelService.save(feature);

                return;
            }
        }
        LOG.info("target product feature does not exist creating");
        ProductFeatureModel newFeature = modelService.create(ProductFeatureModel.class);

        copyFeature(productFeatureModel, newFeature, target, targetClassificationCatalog, targetClassificationCatalogVersion);

        newFeature.setProduct(product);
        newFeature.setClassificationAttributeAssignment(target);
        newFeature.setQualifier(String.format("%s/%s/%s.%s", targetClassificationCatalog.getId(), targetClassificationCatalogVersion.getVersion(), target.getClassificationClass().getCode(), target.getClassificationAttribute().getCode()));
        newFeature.setLanguage(productFeatureModel.getLanguage());

        modelService.save(newFeature);

    }

    private boolean checkSyncConditions(ClassificationSystemModel sourceClassificationCatalog, ClassificationSystemModel targetClassificationCatalog, ProductFeatureModel productFeatureModel, ClassAttributeAssignmentModel target) {
        if (ClassificationSystemVisibilityEnum.PUBLIC.equals(sourceClassificationCatalog.getVisibility())) {
            LOG.error("Cannot sync product feature from a public classification catalog");
            return false;
        }
        if (ClassificationSystemVisibilityEnum.PRIVATE.equals(targetClassificationCatalog.getVisibility())) {
            LOG.error("Cannot sync product feature to a private classification catalog");
            return false;
        }
        if (!productFeatureModel.getClassificationAttributeAssignment().getMultiValued().equals(target.getMultiValued())) {
            LOG.error("Cannot sync product feature due to a mismatch between the source and target multi-value setting");
            return false;
        }

        return true;
    }

    private void copyFeature(ProductFeatureModel sourceFeature, ProductFeatureModel targetFeature, ClassAttributeAssignmentModel target, ClassificationSystemModel targetClassificationCatalog, ClassificationSystemVersionModel targetClassificationCatalogVersion) {
        targetFeature.setAuthor(sourceFeature.getAuthor());
        targetFeature.setDependantProductFeatures(cloneDependantProductFeatures(sourceFeature, targetFeature, target, targetClassificationCatalog, targetClassificationCatalogVersion));
        targetFeature.setDescription(sourceFeature.getDescription());
        targetFeature.setFeaturePosition(sourceFeature.getFeaturePosition());
        targetFeature.setUnit(sourceFeature.getUnit());
        targetFeature.setValue(sourceFeature.getValue());
        targetFeature.setValueDetails(sourceFeature.getValueDetails());
        targetFeature.setValuePosition(sourceFeature.getValuePosition());
    }

    private List<DependantProductFeatureModel> cloneDependantProductFeatures(ProductFeatureModel sourceFeature, ProductFeatureModel targetFeature, ClassAttributeAssignmentModel target, ClassificationSystemModel targetClassificationCatalog, ClassificationSystemVersionModel targetClassificationCatalogVersion) {
        if (CollectionUtils.isNotEmpty(sourceFeature.getDependantProductFeatures())) {
            List<DependantProductFeatureModel> dependantFeatureList = new ArrayList<>();
            for (DependantProductFeatureModel sourceDependant : sourceFeature.getDependantProductFeatures()) {

                DependantProductFeatureModel targetDependant = modelService.create(DependantProductFeatureModel.class);
                copyDependantFeature(sourceDependant, targetDependant);

                targetDependant.setParentProductFeature(targetFeature);
                targetDependant.setProduct(targetFeature.getProduct());
                targetDependant.setQualifier(String.format("%s/%s/%s.%s", targetClassificationCatalog.getId(), targetClassificationCatalogVersion.getVersion(), target.getClassificationClass().getCode(), target.getClassificationAttribute().getCode()));

                dependantFeatureList.add(targetDependant);
            }

            return dependantFeatureList;
        }
        return null;
    }

    private void copyDependantFeature(DependantProductFeatureModel sourceDependant, DependantProductFeatureModel targetDependant) {
        targetDependant.setClassificationAttribute(sourceDependant.getClassificationAttribute());
        targetDependant.setDescription(sourceDependant.getDescription());
        targetDependant.setUnit(sourceDependant.getUnit());
        targetDependant.setValue(sourceDependant.getValue());
        targetDependant.setValueDetails(sourceDependant.getValueDetails());
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}
