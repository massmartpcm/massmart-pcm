package za.co.massmart.core.classification.impl.model;

import com.google.gson.JsonObject;

public class SiloImage {

    private static final String PROPERTY_ID = "uid";
    private static final String PROPERTY_LARGE_URL = "large";
    private static final String PROPERTY_HIGHRES_URL = "hires";
    private static final String PROPERTY_CREATED = "created";

    private String view;
    private JsonObject image;
    private boolean primary;

    public SiloImage(String view, JsonObject image) {
        this.view = view;
        this.image = image;
        this.primary = false;
    }

    public String getId() {
        return image.get(PROPERTY_ID).getAsString();
    }

    public String getLargeUrl() {
        return image.get(PROPERTY_LARGE_URL).getAsString();
    }

    public String getHighResUrl() {
        if (!image.has(PROPERTY_HIGHRES_URL)) {
            return null;
        }
        return image.get(PROPERTY_HIGHRES_URL).getAsString();
    }

    public String getWhenCreated() {
        return image.get(PROPERTY_CREATED).getAsString();
    }

    public String getView() {
        return view;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }
}
