package za.co.massmart.core.impex.jalo.media;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.media.Media;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.log4j.Logger;


/**
 * MediaDataTranslator which adds an additional 'external-url:' prefix which imports the media from a URL Inputstream.
 *
 * @author garyb
 *
 */
public class MediaDataTranslator extends de.hybris.platform.impex.jalo.media.MediaDataTranslator
{
    private static final Logger LOG = Logger.getLogger(MediaDataTranslator.class);
    private final static String PREFIX_EXTERNAL_URL = "external-url:";

    private final boolean legacyMode = Config.getBoolean("impex.legacy.mode", true);
    private MediaService mediaService;
    private ModelService modelService;

    /**
     * Override the default performImport to handle <code>cellValue</code>s prefixed with 'external_url:', passing other
     * prefixes to {@link de.hybris.platform.impex.jalo.media.MediaDataTranslator#performImport(String, Item)}
     *
     * @see de.hybris.platform.impex.jalo.media.MediaDataTranslator#performImport(java.lang.String,
     *      de.hybris.platform.jalo.Item)
     */
    @Override
    public void performImport(final String cellValue, final Item processedItem) throws ImpExException
    {
        if (isExternalUrl(cellValue))
        {
            setDataFromExternalUrl((Media) processedItem, cellValue);
        }
        else
        {
            super.performImport(cellValue, processedItem);
        }
    }

    protected boolean isExternalUrl(final String path)
    {
        return path.toLowerCase().startsWith(PREFIX_EXTERNAL_URL);
    }

    protected void setDataFromExternalUrl(final Media media, final String path) throws ImpExException
    {
        final String url = removeControlPrefixFromPath(PREFIX_EXTERNAL_URL, path);
        try
        {
            final InputStream input = new URL(url).openStream();
            setStreamForMedia(media, input);
        }
        catch (final IOException e)
        {
            LOG.error("Error importing external-url for path: " + path, e);
            throw new ImpExException("Invalid external-url definition!");
        }
    }

    protected void setStreamForMedia(final Media media, final InputStream dataStream)
    {
        if (this.legacyMode)
        {
            media.setData(dataStream, media.getRealFileName(), media.getMime());
        }
        else
        {
            final MediaModel mediaModel = (MediaModel) getModelService().get(media);
            getMediaService().setStreamForMedia(mediaModel, dataStream, mediaModel.getRealFileName(), mediaModel.getMime());
        }
    }

    private String removeControlPrefixFromPath(final String controlPrefix, final String fullPath)
    {
        return fullPath.substring(controlPrefix.length(), fullPath.length()).trim();
    }

    protected MediaService getMediaService()
    {
        if (this.mediaService == null)
        {
            this.mediaService = (Registry.getApplicationContext().getBean("mediaService", MediaService.class));
        }

        return this.mediaService;
    }

    protected ModelService getModelService()
    {
        if (this.modelService == null)
        {
            this.modelService = (Registry.getApplicationContext().getBean("modelService", ModelService.class));
        }

        return this.modelService;
    }

}