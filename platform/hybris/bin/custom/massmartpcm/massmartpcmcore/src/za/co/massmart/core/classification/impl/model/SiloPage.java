package za.co.massmart.core.classification.impl.model;

import com.google.gson.JsonObject;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SiloPage {

    private static final String PROPERTY_PRODUCT_COUNT = "product_count";
    private static final String PROPERTY_PRODUCTS_PER_PAGE = "products_per_page";
    private static final String PROPERTY_PAGE_COUNT = "page_count";
    private static final String PROPERTY_PAGE_NUMBER = "page";
    private static final String PROPERTY_PRODUCT = "product";

    private JsonObject page;

    public SiloPage(JsonObject page) {
        this.page = page;
    }

    public int getTotalNumberOfProducts() {
        return page.get(PROPERTY_PRODUCT_COUNT).getAsInt();
    }

    public int getNumberOfProductsPerPage() {
        return page.get(PROPERTY_PRODUCTS_PER_PAGE).getAsInt();
    }

    public int getNumberOfPages() {
        if (page.has(PROPERTY_PAGE_COUNT)) {
            return page.get(PROPERTY_PAGE_COUNT).getAsInt();
        } else {
            return 0;
        }
    }

    public int getPageNumber() {
        return page.get(PROPERTY_PAGE_NUMBER).getAsInt();
    }

    public JsonObject getPage() {
        return page;
    }

    public void setPage(JsonObject page) {
        this.page = page;
    }

    public List<SiloProduct> getProducts() {
        if (!page.has(PROPERTY_PRODUCT) || page.get(PROPERTY_PRODUCT).isJsonPrimitive()) {
            return Collections.emptyList();
        } else {
            JsonObject productSection = page.getAsJsonObject(PROPERTY_PRODUCT);
            return getAllPropertiesAsJsonObjects(productSection).stream()
                    .map(SiloProduct::new)
                    .collect(Collectors.toList());
        }
    }

    protected List<JsonObject> getAllPropertiesAsJsonObjects(JsonObject jsonObject) {
        return getAllPropertyNames(jsonObject).stream()
                .map(memberName -> (JsonObject) jsonObject.get(memberName))
                .collect(Collectors.toList());
    }

    protected List<String> getAllPropertyNames(JsonObject jsonObject) {
        return jsonObject.entrySet().stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}