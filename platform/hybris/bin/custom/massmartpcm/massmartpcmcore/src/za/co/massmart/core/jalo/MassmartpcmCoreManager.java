/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package za.co.massmart.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;

import de.hybris.platform.catalog.jalo.ProductFeature;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import za.co.massmart.core.jalo.catalog.DependantProductFeature;
import za.co.massmart.core.constants.MassmartpcmCoreConstants;
import za.co.massmart.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
@SuppressWarnings("PMD")
public class MassmartpcmCoreManager extends GeneratedMassmartpcmCoreManager
{
	public static final MassmartpcmCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MassmartpcmCoreManager) em.getExtension(MassmartpcmCoreConstants.EXTENSIONNAME);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * za.co.builders.core.jalo.GeneratedMbCoreManager#getDependantProductFeatures(de.hybris.platform.jalo.SessionContext
	 * , de.hybris.platform.catalog.jalo.ProductFeature)
	 */
	@Override
	public List<DependantProductFeature> getDependantProductFeatures(final SessionContext ctx, final ProductFeature item)
	{
		return FlexibleSearch
				.getInstance()
				.search(
						(new StringBuilder("SELECT {")).append(Item.PK).append("} FROM {")
								.append(MassmartpcmCoreConstants.TC.DEPENDANTPRODUCTFEATURE).append("} ").append("WHERE {")
								.append("parentProductFeature").append("}= ?productFeature ").toString(),
						Collections.singletonMap("productFeature", item.getPK()), DependantProductFeature.class).getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * za.co.builders.core.jalo.GeneratedMbCoreManager#setDependantProductFeatures(de.hybris.platform.jalo.SessionContext
	 * , de.hybris.platform.catalog.jalo.ProductFeature, java.util.List)
	 */
	@Override
	public void setDependantProductFeatures(final SessionContext ctx, final ProductFeature productFeature,
											final List<DependantProductFeature> dependantFeatures)
	{
		if (CollectionUtils.isNotEmpty(dependantFeatures))
		{
			for (final Iterator iterator = dependantFeatures.iterator(); iterator.hasNext();)
			{
				final DependantProductFeature dependantFeature = (DependantProductFeature) iterator.next();
				dependantFeature.setProperty(ctx, "parentProductFeature", productFeature);
				//						if (!productFeature.equals(dependantFeature.getParentProductFeature(ctx)))
				//						{
				//							throw new JaloInvalidParameterException((new StringBuilder("dependant feature ")).append(dependantFeature)
				//									.append(" of ").append(dependantFeatures).append(" does not belong to productFeature ")
				//									.append(productFeature).toString(), 0);
				//						}
			}

		}
		Collection toRemove = getDependantProductFeatures(ctx, productFeature);
		if (CollectionUtils.isNotEmpty(toRemove))
		{
			if (CollectionUtils.isNotEmpty(dependantFeatures))
			{
				toRemove = new HashSet(toRemove);
				toRemove.removeAll(dependantFeatures);
			}
			for (final Iterator iterator1 = toRemove.iterator(); iterator1.hasNext();)
			{
				final DependantProductFeature feature = (DependantProductFeature) iterator1.next();
				try
				{
					feature.remove(ctx);
				}
				catch (final ConsistencyCheckException e)
				{
					e.printStackTrace();
				}
			}

		}

	}
}
