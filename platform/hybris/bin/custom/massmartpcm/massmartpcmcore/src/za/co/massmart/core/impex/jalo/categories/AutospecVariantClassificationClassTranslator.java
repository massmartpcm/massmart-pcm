package za.co.massmart.core.impex.jalo.categories;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * Translator which sets the billing address on a b2b unit.
 *
 * @author chriss
 *
 */
public class AutospecVariantClassificationClassTranslator extends AbstractSpecialValueTranslator
{
    private static final Logger LOG = Logger.getLogger(AutospecVariantClassificationClassTranslator.class);

    private ModelService modelService;
    private CatalogVersionService catalogVersionService;
    private DefaultProductService productService;

    @Override
    public void performImport(final String cellValue, final Item processedItem)
    {

        if (processedItem != null)
        {
            final String[] cellValueArray = cellValue != null ? cellValue.split(":") : null;
            LOG.info("cell value array contains : " + cellValueArray.length);

            final ClassificationClassModel classificationClass = getModelService().get(processedItem.getPK());
            LOG.info("ClassificationClassModel = " + classificationClass.getCode());

            final String articleId = cellValueArray[0];
            final String catalogName = cellValueArray[1];
            final String catalogVersionName = cellValueArray[2];

            final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(catalogName, catalogVersionName);

            final ProductModel product = getProductService().getProductForCode(catalogVersion, articleId);

            if (product != null && product instanceof GenericVariantProductModel)
            {
                //				final GenericVariantProductModel genericVariantProduct = (GenericVariantProductModel) product;
                if (!classificationClass.getProducts().contains(product))
                {
                    final List<ProductModel> products = new ArrayList<ProductModel>();
                    products.addAll(classificationClass.getProducts());
                    products.add(product);
                    classificationClass.setProducts(products);
                    modelService.save(classificationClass);
                }
            }
        }
    }

    @Override
    public String performExport(final Item processedItem)
    {
        return "";
    }


    protected ModelService getModelService()
    {
        if (this.modelService == null)
        {
            this.modelService = (Registry.getApplicationContext().getBean("modelService", ModelService.class));
        }

        return this.modelService;
    }

    protected CatalogVersionService getCatalogVersionService()
    {
        if (this.catalogVersionService == null)
        {
            this.catalogVersionService = (Registry.getApplicationContext().getBean("catalogVersionService",
                    CatalogVersionService.class));
        }

        return this.catalogVersionService;
    }

    public DefaultProductService getProductService()
    {
        if (this.productService == null)
        {
            this.productService = (Registry.getApplicationContext().getBean("defaultProductService", DefaultProductService.class));
        }

        return this.productService;
    }

}