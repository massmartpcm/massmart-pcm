package za.co.massmart.core.interceptors;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import za.co.massmart.core.enums.ClassificationSystemVisibilityEnum;

import java.util.List;

public class PCMProductFeatureRemoveInterceptor implements RemoveInterceptor<ProductFeatureModel> {

    private static final Logger LOG = Logger.getLogger(PCMProductFeatureRemoveInterceptor.class);

    private ModelService modelService;

    @Override
    public void onRemove(ProductFeatureModel productFeatureModel, InterceptorContext interceptorContext) throws InterceptorException {
        ProductModel product = productFeatureModel.getProduct();
        List<ProductFeatureModel> features = product.getFeatures();

        String sourceName = productFeatureModel.getClassificationAttributeAssignment().getClassificationAttribute().getName();
        ClassificationSystemModel sourceClassificationCatalog = productFeatureModel.getClassificationAttributeAssignment().getClassificationClass().getCatalogVersion().getCatalog();
        ClassificationSystemVersionModel sourceClassificationCatalogVersion = productFeatureModel.getClassificationAttributeAssignment().getClassificationClass().getCatalogVersion();

        ClassAttributeAssignmentModel classificationAttributeAssignment;
        if ((classificationAttributeAssignment = productFeatureModel.getClassificationAttributeAssignment()) != null) {
            ClassAttributeAssignmentModel target;
            if ((target = classificationAttributeAssignment.getSyncTarget()) != null) {

                ClassificationSystemModel targetClassificationCatalog = target.getClassificationClass().getCatalogVersion().getCatalog();
                ClassificationSystemVersionModel targetClassificationCatalogVersion = target.getClassificationClass().getCatalogVersion();
                String targetName = target.getClassificationAttribute().getName();

                LOG.info(String.format("Product feature %s:%s [%s] is mapped to %s:%s [%s]", sourceClassificationCatalog.getId(), sourceClassificationCatalogVersion.getVersion(), sourceName, targetClassificationCatalog.getId(), targetClassificationCatalogVersion.getVersion(), targetName));

                if (checkSyncConditions(sourceClassificationCatalog, targetClassificationCatalog, productFeatureModel, target)) {

                    syncProductFeature(features, target);

                }
            }
        }
    }

    private void syncProductFeature(List<ProductFeatureModel> features, ClassAttributeAssignmentModel target) {
        //TODO support for multivalue features

        for (ProductFeatureModel feature : features) {
            if (feature.getClassificationAttributeAssignment().equals(target)) {
                LOG.info("removing target product feature where source feature has been removed");

                modelService.remove(feature);

                break;
            }
        }
    }

    private boolean checkSyncConditions(ClassificationSystemModel sourceClassificationCatalog, ClassificationSystemModel targetClassificationCatalog, ProductFeatureModel productFeatureModel, ClassAttributeAssignmentModel target) {
        if (ClassificationSystemVisibilityEnum.PUBLIC.equals(sourceClassificationCatalog.getVisibility())) {
            LOG.error("Cannot sync product feature from a public classification catalog");
            return false;
        }
        if (ClassificationSystemVisibilityEnum.PRIVATE.equals(targetClassificationCatalog.getVisibility())) {
            LOG.error("Cannot sync product feature to a private classification catalog");
            return false;
        }
        if (!productFeatureModel.getClassificationAttributeAssignment().getMultiValued().equals(target.getMultiValued())) {
            LOG.error("Cannot sync product feature due to a mismatch between the source and target multi-value setting");
            return false;
        }

        return true;
    }

    protected ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}
