package za.co.massmart.core.file.impl;

import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import za.co.massmart.core.file.FileProcessor;
import za.co.massmart.core.product.services.impl.DefaultProductService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OnecomMediaFileProcessor implements FileProcessor {

    private static final Logger LOG = Logger.getLogger(OnecomMediaFileProcessor.class.getName());
    private static final String PRODUCT_CATALOG = "massmartProductCatalog";
    private static final String PRODUCT_CATALOG_VERSION = "Staged";

    private ModelService modelService;
    private FlexibleSearchService flexibleSearchService;
    private MediaService mediaService;

    private String importPath = "";
    private static String CONVERSION_GROUP = "productImageConversionGroup";
    private static String IMPORT_FORMAT = "Default-ImportFormat";

    private DefaultProductService productService;
    private DefaultCatalogVersionService catalogVersionService;

    @Override
    public boolean processFile(InputStream inputStream, String jobCode, String fileName) throws IOException {

        boolean isProcessingFileSuccess = true;

        List<OnecomMediaCsvRow> inputList = new ArrayList<>();

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        // skip the header of the csv
        inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());

        CatalogVersionModel catalogVersion =  catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, PRODUCT_CATALOG_VERSION);

        ConversionGroupModel defaultConversionGroup = new ConversionGroupModel();
        defaultConversionGroup.setCode(CONVERSION_GROUP);
        ConversionGroupModel conversionGroup = flexibleSearchService.getModelByExample(defaultConversionGroup);

        MediaFormatModel defaultMediaFormat = new MediaFormatModel();
        defaultMediaFormat.setQualifier(IMPORT_FORMAT);
        MediaFormatModel mediaFormat = flexibleSearchService.getModelByExample(defaultMediaFormat);

        int errorCount = 0;
        for (OnecomMediaCsvRow entry : inputList) {

            ProductModel product = productService.fetchProductForBarcode(catalogVersion, entry.getBarcode());

            if (product == null) {
                continue;
            }

            List<MediaContainerModel> galleryImages = product.getGalleryImages();

            List<MediaContainerModel> newGalleryImages = new ArrayList<MediaContainerModel>();

            for (MediaContainerModel mediaContainer:galleryImages) {
                if (!mediaContainer.getQualifier().equals(entry.getItemNumber() + "-container")) {
                    newGalleryImages.add(mediaContainer);
                }
            }

            MediaContainerModel mcm = new MediaContainerModel();
            mcm.setQualifier(entry.getItemNumber() + "-container");
            mcm.setCatalogVersion(catalogVersion);

            try {
                mcm = flexibleSearchService.getModelByExample(mcm);
            } catch (ModelNotFoundException e) {
                mcm = null;
            }

            if (mcm == null) {
                mcm = new MediaContainerModel();
                mcm.setQualifier(entry.getItemNumber() + "-container");
                mcm.setCatalogVersion(catalogVersion);
            }
            mcm.setConversionGroup(conversionGroup);
            mcm.setMedias(createMedia(entry.getItemNumber(), mcm, mediaFormat, catalogVersion));
            mcm.setSource("Onecom");

            modelService.save(mcm);

            newGalleryImages.add(mcm);

            product.setGalleryImages(newGalleryImages);

            modelService.saveAll(product);
        }

        if (errorCount > 0) {
            LOG.info(" Errors in the file, please correct them and re-process the file ");
            isProcessingFileSuccess = false;
            return isProcessingFileSuccess;
        }

        if (br != null) {
            br.close();
        }

        if (inputStream != null) {
            inputStream.close();
        }

        return isProcessingFileSuccess;
    }

    private Function<String, OnecomMediaCsvRow> mapToItem = (line) -> {

        OnecomMediaCsvRow csvRow = new OnecomMediaCsvRow();

        String[] p = line.split(";");// a CSV has comma separated lines

        String itemNumber = p[0];
        String barcode = p[1];
        String brand = p[2];
        String description = p[3];

        csvRow.setItemNumber(itemNumber.trim());
        csvRow.setBarcode(barcode.trim());
        csvRow.setBrand(brand.trim());
        csvRow.setDescription(description.trim());

        return csvRow;
    };

    private List<MediaModel> createMedia(String imageName, MediaContainerModel mediaContainer, MediaFormatModel mediaFormat, CatalogVersionModel catalogVersion) throws FileNotFoundException {

        if (mediaContainer.getMedias() != null && !mediaContainer.getMedias().isEmpty()) {
            modelService.removeAll(mediaContainer.getMedias());
        }

        List<MediaModel> medias = new ArrayList<MediaModel>();

        MediaModel media = new MediaModel();
        media.setCode(imageName);
        media = new MediaModel();
        media.setCode(imageName);
        media.setMediaFormat(mediaFormat);
        media.setCatalogVersion(catalogVersion);
        media.setRealFileName(imageName + "_1200x1200.jpg");
        media.setMime("image/jpeg");

        modelService.save(media);

 //       media.setFolder("images");

        File file = new File(importPath + imageName + "_1200x1200.jpg");
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found " + file.getName());
            throw e;
        }
        mediaService.setStreamForMedia(media, inputStream);
        media.setMediaContainer(mediaContainer);

        medias.add(media);
        return medias;
    }

    public DefaultProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(DefaultProductService productService) {
        this.productService = productService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public MediaService getMediaService() {
        return mediaService;
    }

    @Required
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }

    public DefaultCatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(DefaultCatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public void setImportPath(String importPath) {
        this.importPath = importPath;
    }
}
