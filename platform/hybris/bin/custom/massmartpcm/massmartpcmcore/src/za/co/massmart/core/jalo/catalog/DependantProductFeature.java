package za.co.massmart.core.jalo.catalog;

import de.hybris.platform.catalog.jalo.classification.util.FeatureContainer;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.JaloPropertyContainer;
import de.hybris.platform.util.Utilities;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;


public class DependantProductFeature extends GeneratedDependantProductFeature
{
	@SuppressWarnings("unused")
	private final static Logger LOG = Logger.getLogger(DependantProductFeature.class.getName());

	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
			throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem(ctx, type, allAttributes);
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see za.co.builders.catalog.jalo.GeneratedDependantProductFeature#getValue(de.hybris.platform.jalo.SessionContext)
	 */
	@Override
	public Object getValue(final SessionContext ctx)
	{
		return getRawValue(ctx);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see za.co.builders.catalog.jalo.GeneratedDependantProductFeature#setValue(de.hybris.platform.jalo.SessionContext,
	 * java.lang.Object)
	 */
	@Override
	public void setValue(final SessionContext ctx, final Object value) throws NullPointerException
	{
		if (value == null)
		{
			throw new NullPointerException("value was null");
		}
		doSetSingleValue(ctx, null, value);
	}

	protected void doSetSingleValue(final SessionContext ctx, final JaloPropertyContainer cont, final Object value)
	{
		final boolean initial = cont != null;

		if (initial)
		{
			cont.setProperty("rawValue", value);
		}
		else
		{
			setRawValue(ctx, value);
		}

		setSearchFields(ctx, cont, value);
		if (initial)
		{
			return;
		}
		markProductAsModified();
	}

	protected void setSearchFields(final SessionContext ctx, final JaloPropertyContainer cont, final Object value)
	{
		final boolean initial = cont != null;
		final int valueType = calculateSingleValueType(value);
		if (initial)
		{
			cont.setProperty("valueType", Integer.valueOf(valueType));
		}
		else
		{
			setValueType(valueType);
		}
		String string = null;
		Boolean booleanValue = null;
		BigDecimal bigDecimal = null;
		switch (valueType)
		{
			case 1:
				if (isStringToNumberConversionEnabled())
				{
					try
					{
						bigDecimal = BigDecimal.valueOf(Double.parseDouble((String) value));
					}
					catch (final NumberFormatException localNumberFormatException)
					{
						//
					}
				}

				string = (String) value;
				booleanValue = ("true".equalsIgnoreCase((String) value)) ? Boolean.TRUE : Boolean.FALSE;
				break;
			case 2:
				booleanValue = (Boolean) value;
				string = ((Boolean) value).toString();
				bigDecimal = BigDecimal.valueOf((Boolean.TRUE.equals(value)) ? 1.0D : 0.0D);
				break;
			case 3:
				bigDecimal = convertToBigDecimal((Number) value);
				booleanValue = (0.0D != ((Number) value).doubleValue()) ? Boolean.TRUE : Boolean.FALSE;
				string = ((Number) value).toString();
				break;
			case 5:
				bigDecimal = BigDecimal.valueOf((value != null) ? ((Date) value).getTime() : 0L);
				booleanValue = (value != null) ? Boolean.TRUE : Boolean.FALSE;
				string = (value != null) ? Utilities.getDateTimeInstance().format((Date) value) : null;
				break;
			case 4:
				string = ((Item) value).getPK().toString();
				booleanValue = Boolean.TRUE;
				bigDecimal = BigDecimal.valueOf(1.0D);
				break;
			default:
				throw new JaloInvalidParameterException("invalid value type " + valueType + " for value " + value, 0);
		}
		if (initial)
		{
			cont.setProperty("stringValue", string);
			cont.setProperty("booleanValue", booleanValue);
			cont.setProperty("numberValue", bigDecimal);
		}
		else
		{
			setStringValue(ctx, string);
			setBooleanValue(ctx, booleanValue);
			setNumberValue(ctx, bigDecimal);
		}
	}

	protected BigDecimal convertToBigDecimal(final Number number)
	{
		if ((number == null) || (number instanceof BigDecimal))
		{
			return ((BigDecimal) number);
		}

		return new BigDecimal(number.doubleValue());
	}

	private void markProductAsModified()
	{
		if ((getProduct() == null)
				|| (FeatureContainer.isInFeatureContainerTA(JaloSession.getCurrentSession().getSessionContext())))
		{
			return;
		}
		getProduct().setModificationTime(new Date());
	}

	protected boolean isStringToNumberConversionEnabled()
	{
		return Registry.getCurrentTenant().getConfig().getBoolean("classification.enable.string.to.number.conversion", true);
	}

	protected int calculateSingleValueType(final Object value)
	{
		if (value == null)
		{
			throw new NullPointerException("value cannot be null");
		}
		if (value instanceof Item)
		{
			return 4;
		}
		if (value instanceof Boolean)
		{
			return 2;
		}
		if (value instanceof Number)
		{
			return 3;
		}
		if (value instanceof String)
		{
			return 1;
		}
		if (value instanceof Date)
		{
			return 5;
		}

		throw new JaloInvalidParameterException("invalid value " + value + " (class=" + value.getClass().getName()
				+ ") - cannot get value type", 0);
	}

}