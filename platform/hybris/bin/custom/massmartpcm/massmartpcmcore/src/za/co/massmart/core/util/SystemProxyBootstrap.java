package za.co.massmart.core.util;

import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * Bootstrap Proxy Setting
 *
 * @author garyb
 */
public class SystemProxyBootstrap {
    public void setSystemProxy() {
        if (Config.getBoolean("proxy.useproxy", false)) {
            System.setProperty("http.proxyHost", Config.getString("proxy.host", null));
            System.setProperty("https.proxyHost", Config.getString("proxy.host", null));
            System.setProperty("ftp.proxyHost", Config.getString("proxy.host", null));

            System.setProperty("http.proxyPort", Config.getString("proxy.port", "8080"));
            System.setProperty("https.proxyPort", Config.getString("proxy.port", "8080"));
            System.setProperty("ftp.proxyPort", Config.getString("proxy.port", "8080"));

            System.setProperty("http.nonProxyHosts", Config.getString("proxy.nonProxyHosts", null));
            System.setProperty("https.nonProxyHosts", Config.getString("proxy.nonProxyHosts", null));
            System.setProperty("ftp.nonProxyHosts", Config.getString("proxy.nonProxyHosts", null));

            final String authUser = Config.getString("proxy.user", null);

            if (StringUtils.isNotBlank(authUser)) {
                System.setProperty("http.proxyUser", authUser);
                System.setProperty("https.proxyUser", authUser);
                System.setProperty("ftp.proxyUser", authUser);

                final String authPassword = Config.getString("proxy.password", null);

                if (StringUtils.isNotBlank(authPassword)) {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        public PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(authUser, authPassword.toCharArray());
                        }
                    });
                    System.setProperty("http.proxyPassword", authPassword);
                    System.setProperty("https.proxyPassword", authPassword);
                    System.setProperty("ftp.proxyPassword", authPassword);
                }
            }
        }
    }
}