package za.co.massmart.core.file;

import java.io.IOException;
import java.io.InputStream;

public interface FileProcessor {


    /**
     * Process the uploaded file and creates the Merchandising config entries
     * if all the entries are validated and successful
     * @param inputStream
     * @param jobCode
     * @param fileName
     * @return
     * @throws IOException
     */
    boolean processFile(InputStream inputStream, String jobCode, String fileName) throws IOException;

    void setImportPath(String importPath);

}
