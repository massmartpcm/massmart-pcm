package za.co.massmart.core.classification.service.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.UnlocalizedFeature;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import za.co.massmart.core.classification.impl.model.SiloProduct;
import za.co.massmart.core.product.services.ProductService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SiloProductProcessor {

    public static final String SILO_LABEL_PROPERTY = "label";
    public static final String SILO_SUB_LABEL_PROPERTY = "sub_label";
    public static final String SILO_TITLE_LABEL = "title";

    private static final Logger log = LoggerFactory.getLogger(SiloProductProcessor.class);

    private Set<String> primitiveStyleAttributes;
    private Set<String> objectStyleAttributes;
    private Set<String> listStyleAttributes;
    private Set<String> structuredAttributes;
    private Set<String> ignoredAttributes;

    private ClassificationService classificationService;
    private ModelService modelService;
    private ProductService productService;

    /**
     * Creates a classification and feature structure for a product represented by a JSON structure from Silo.
     * <p>
     * The Silo product structure contains an EAN for the product. This method will look up a product with that EAN
     * in Hybris, in the same catalog as <tt>classificationRoot</tt>. If such a product is found, the features described
     * by the Silo product attributes will be added to that product. If the corresponding classification structure
     * for the product does not exist, it will also be created.
     * <p>
     * Note that this method will remove all existing features from the product in question.
     *
     * @param siloProduct        incoming product data from Silo
     * @param classificationRoot the classification class which forms the root of the classification structure.
     * @return the product to which features were added as a result of the call, or <tt>null</tt> if no matching
     * product could be determined.
     */
    public ProductModel processSiloProductAndAddClassifications(CatalogVersionModel catalogVersionModel, SiloProduct siloProduct, ClassificationClassModel classificationRoot) {
        String barcode = siloProduct.getBarcode();
        if (StringUtils.isEmpty(barcode)) {
            return null;
        }

        ProductModel product;
        try {
            boolean activeOnly = catalogVersionModel == null;
            product = productService.fetchProductForBarcode(catalogVersionModel, barcode);
        } catch (AmbiguousIdentifierException e) {
            log.warn("Multiple products found with EAN " + barcode, e);
            return null;
        }
        if (product == null) {
            return null;
        } else if (log.isInfoEnabled()) {
            log.info("Found product for EAN " + barcode + ": " + product.getCode() + " (" + product.getName() + ")");
        }

        //Clear the product's features in the classification category being loaded
        for (ProductFeatureModel productFeature:product.getFeatures()) {
            if (productFeature.getClassificationAttributeAssignment().getClassificationClass().getCatalogVersion().equals(classificationRoot.getCatalogVersion())) {
                modelService.remove(productFeature);
            }
        }
//        modelService.removeAll(product.getFeatures());
        //We need to refresh here as sometimes a product has appeared multiple times in a single run.
        //This causes duplication, unless this refresh call happens to make the effects of the removeAll
        //call above visible.
        modelService.refresh(product);

//        if (StringUtils.isNotEmpty(siloProduct.getDescription())) {
//            product.setDescription(siloProduct.getDescription());
//            modelService.save(product);
//        }

        List<String> members = getAllPropertyNames(siloProduct.getJsonObject());
        for (String member : members) {
            if (!ignoredAttributes.contains(member)) {
                if (getAllAttributesToProcess().contains(member)) {
                    if (!siloProduct.getJsonObject().get(member).isJsonNull()) {
                        if (primitiveStyleAttributes.contains(member)) {
                            processPrimitiveStyleProperty(product, member, siloProduct.getJsonObject().getAsJsonPrimitive(member), classificationRoot);
                        } else if (objectStyleAttributes.contains(member)) {
                            ClassificationClassModel classification = getOrCreateClassificationClass(member, classificationRoot);
                            processObjectStyleProperty(product, siloProduct.getJsonObject().getAsJsonObject(member), classification);
                        } else if (structuredAttributes.contains(member)) {
                            ClassificationClassModel classification = getOrCreateClassificationClass(member, classificationRoot);
                            processLabelStyleProperty(product, siloProduct.getJsonObject().getAsJsonObject(member), classification);
                        } else if (listStyleAttributes.contains(member)) {
                            ClassificationClassModel classification = getOrCreateClassificationClass(member, classificationRoot);
                            processListStyleProperty(product, siloProduct.getJsonObject().getAsJsonObject(member), classification);
                        }
                    }
                } else {
                    log.warn("Unknown property: " + member + " on product " + siloProduct.getBarcode());
                }
            }
        }

        return product;
    }

    /**
     * Processes a list-style Silo property for a given product, described by a given <tt>JsonObject</tt>. If the classification
     * structure needed to express the property does not exist, it will be created.
     * <p>
     * A list-style property (such as "ingredients") contains a flat list of attributes that should be added to the product
     * within a single classification attribute.
     * <p>
     * This method will traverse through all properties of the JSON object recursively and create product attributes based on
     * any <tt>JsonArray</tt>s it finds. This is to allow for some flexibility when processing different sorts of structures
     * from Silo.
     *
     * @param product             the product to which the features described in <tt>propertyJsonObject</tt> should be added
     * @param propertyJsonObject  Silo's JSON-based definition of the product features
     * @param classificationClass the classification class that should contain the features
     */
    protected void processListStyleProperty(ProductModel product, JsonObject propertyJsonObject, ClassificationClassModel classificationClass) {
        for (String propertyName : getAllPropertyNames(propertyJsonObject)) {
            if (propertyJsonObject.get(propertyName).isJsonObject()) {
                processListStyleProperty(product, propertyJsonObject.get(propertyName).getAsJsonObject(), classificationClass);
            } else if (propertyJsonObject.get(propertyName).isJsonArray()) {

                ClassAttributeAssignmentModel classificationAttributeAssignment = getOrCreateClassificationAttributeAssignment(propertyName, classificationClass, true);

                JsonArray listJson = propertyJsonObject.getAsJsonArray(propertyName);
                String[] list = IntStream.range(0, listJson.size())
                        .mapToObj(i -> listJson.get(i).getAsString())
                        .toArray(String[]::new);

                addFeatureToProduct(product, classificationAttributeAssignment, list);
            }
        }
    }

    /**
     * Processes a primitive-style property for a given product, described by a <tt>propertyName</tt> and <tt>JsonPrimitive</tt>. If the
     * classification structure needed to express the property does not exist, it will be created.
     * <p>
     * A primitive-style property is a JSON primitive (like a String) that holds a value directly.
     *
     * @param product             the product to which the feature described in <tt>jsonPrimitive</tt> should be added
     * @param propertyName        the name of the property that holds this value
     * @param jsonPrimitive       a JSON primitive representing the value
     * @param classificationClass the classification class that should contain the feature
     */
    protected void processPrimitiveStyleProperty(ProductModel product, String propertyName, JsonPrimitive jsonPrimitive, ClassificationClassModel classificationClass) {
        ClassAttributeAssignmentModel classificationAttributeAssignment = getOrCreateClassificationAttributeAssignment(propertyName, classificationClass, false);
        addFeatureToProduct(product, classificationAttributeAssignment, jsonPrimitive.getAsString());
    }

    /**
     * Processes an object-style property for a given product, described by a <tt>JsonObject</tt>. If the classification structure needed
     * to express the property does not exist, it will be created.
     *
     * An object-style property is a JSON object whose properties are either JSON objects or primitives. This method will traverse through all
     * properties of the object recursively and create product attributes based on the primitives it finds.
     *
     * @param product the product to which the features described in <tt>propertyJsonObject</tt> should be added
     * @param propertyJsonObject Silo's JSON-based definition of the product features
     * @param classificationClass the classification class that should contain the features
     */
    protected void processObjectStyleProperty(ProductModel product, JsonObject propertyJsonObject, ClassificationClassModel classificationClass) {
        for (String propertyName : getAllPropertyNames(propertyJsonObject)) {
            if (propertyJsonObject.get(propertyName).isJsonObject()) {
                processObjectStyleProperty(product, propertyJsonObject.get(propertyName).getAsJsonObject(), classificationClass);
            } else if (propertyJsonObject.get(propertyName).isJsonPrimitive()) {
                ClassAttributeAssignmentModel classificationAttributeAssignment = getOrCreateClassificationAttributeAssignment(propertyName, classificationClass, false);
                addFeatureToProduct(product, classificationAttributeAssignment, propertyJsonObject.getAsJsonPrimitive(propertyName).getAsString());
            }
        }
    }

    /**
     * Processes a label-style Silo property for a given product, described by a given <tt>JsonObject</tt>. If the classification
     * structure needed to express the property does not exist, it will be created.
     * <p>
     * A label-style property (such as "nutrition info") contains a structured list of attributes (such as the amount of fat,
     * and within that, the amount of trans-fat, etc.) that should be added to the product, in a tree of separate classification
     * attributes.
     * <p>
     * This method will traverse through a structure of "label" and "sub-label" properties of the JSON object recursively
     * and create product attributes based on the leaves of this structure.
     *
     * @param product               the product to which the features described in <tt>propertyJsonObject</tt> should be added
     * @param propertyJsonObject    Silo's JSON-based definition of the product features
     * @param currentClassification the classification class that should contain the top-level features created
     */
    protected void processLabelStyleProperty(ProductModel product, JsonObject propertyJsonObject, ClassificationClassModel currentClassification) {
        List<String> properties = getAllPropertyNames(propertyJsonObject);
        for (String property : properties) {
            if ((SILO_LABEL_PROPERTY.equals(property) || SILO_SUB_LABEL_PROPERTY.equals(property)) && propertyJsonObject.get(property).isJsonArray()) {
                createLabelAndSubLabelStructure(product, propertyJsonObject.getAsJsonArray(property), currentClassification);
            } else if (propertyJsonObject.get(property).isJsonObject()) {
                ClassificationClassModel classification = getOrCreateClassificationClass(property, currentClassification);
                processLabelStyleProperty(product, (JsonObject) propertyJsonObject.get(property), classification);
            }
        }
    }

    protected void createLabelAndSubLabelStructure(ProductModel product, JsonArray labels, ClassificationClassModel parentClassification) {
        for (JsonElement label : labels) {
            String title = label.getAsJsonObject().get(SILO_TITLE_LABEL).getAsString();
            ClassificationClassModel labelClassification = getOrCreateClassificationClass(title, parentClassification);

            List<String> propertyNames = getAllPropertyNames(label.getAsJsonObject());
            for (String propertyName : propertyNames) {
                if (!SILO_TITLE_LABEL.equals(propertyName)) {
                    JsonElement property = label.getAsJsonObject().get(propertyName);
                    if (property.isJsonArray()) {
                        createLabelAndSubLabelStructure(product, property.getAsJsonArray(), labelClassification);
                    } else if (property.isJsonPrimitive()) {
                        ClassAttributeAssignmentModel classificationAttributeAssignment = getOrCreateClassificationAttributeAssignment(propertyName, labelClassification, false);
                        addFeatureToProduct(product, classificationAttributeAssignment, property.getAsString());
                    }
                }
            }
        }
    }

    /**
     * Adds a single or multi-valued feature for a classification attribute to a product.
     *
     * @param product                 the product to which the feature should be added
     * @param classificationAttributeAssignment the classification attribute defining the feature
     * @param values                  the value(s) of the feature. If one value is supplied, the value
     *                                will be set directly. Otherwise a list will be set.
     */
    protected void addFeatureToProduct(ProductModel product, ClassAttributeAssignmentModel classificationAttributeAssignment, String... values) {
        if (log.isDebugEnabled()) {
            log.debug("Adding feature to " + product.getName() + " (" + product.getCode() + "): " + classificationAttributeAssignment.getClassificationAttribute().getName());
        }
        addProductToClassificationClass(product, classificationAttributeAssignment.getClassificationClass());

        List<FeatureValue> featureValues = Arrays.stream(values).map(FeatureValue::new).collect(Collectors.toList());
        classificationService.setFeature(product, new UnlocalizedFeature(classificationAttributeAssignment, featureValues));
    }

    protected void addProductToClassificationClass(ProductModel product, ClassificationClassModel classificationClass) {
        ProductModel baseProduct = getBaseProduct(product);
        if (!baseProduct.getClassificationClasses().contains(classificationClass)) {
            Collection<CategoryModel> supercategories = new ArrayList<>(baseProduct.getSupercategories());
            supercategories.add(classificationClass);
            baseProduct.setSupercategories(supercategories);
            modelService.save(baseProduct);
        }
    }

    protected ProductModel getBaseProduct(ProductModel product) {
        return product instanceof VariantProductModel ? ((VariantProductModel) product).getBaseProduct() : product;
    }

    protected ClassificationClassModel getOrCreateClassificationClass(String name, ClassificationClassModel parent) {
        ClassificationClassModel childClassificationClass = getChildClassification(parent, name);
        if (childClassificationClass == null) {
            childClassificationClass = createClassificationClass(parent, name);
        }
        return childClassificationClass;
    }

    protected ClassAttributeAssignmentModel getOrCreateClassificationAttributeAssignment(String attributeName, ClassificationClassModel classificationClass, boolean multiValue) {
        ClassAttributeAssignmentModel classificationAttributeAssignment = getClassificationAttributeAssignment(classificationClass, attributeName);
        if (classificationAttributeAssignment == null) {
            classificationAttributeAssignment = createClassificationAttributeAndAssignment(classificationClass, attributeName, multiValue);
        }
        return classificationAttributeAssignment;
    }

    protected ClassificationClassModel getChildClassification(ClassificationClassModel parent, String childClassificationName) {
        Collection<CategoryModel> subcategories = parent.getCategories();
        for (CategoryModel subcategory : subcategories) {
            if (subcategory instanceof ClassificationClassModel) {
                if (subcategory.getCode().endsWith("/" + childClassificationName)) {
                    return (ClassificationClassModel) subcategory;
                }
            }
        }
        return null;
    }

    protected ClassAttributeAssignmentModel getClassificationAttributeAssignment(ClassificationClassModel classificationClass, String name) {
        List<ClassAttributeAssignmentModel> classificationAttributeAssignments = classificationClass.getDeclaredClassificationAttributeAssignments();
        for (ClassAttributeAssignmentModel classificationAttributeAssignment : classificationAttributeAssignments) {
            if (classificationAttributeAssignment.getClassificationAttribute().getCode().endsWith("/" + name)) {
                return classificationAttributeAssignment;
            }
        }
        return null;
    }

    protected ClassificationClassModel createClassificationClass(ClassificationClassModel parent, String name) {
        ClassificationClassModel classificationClass = modelService.create(ClassificationClassModel.class);
        classificationClass.setCatalogVersion(parent.getCatalogVersion());
        classificationClass.setCode(parent.getCode() + "/" + name);
        classificationClass.setName(name);
        classificationClass.setSupercategories(Collections.singletonList(parent));
        modelService.save(classificationClass);
        modelService.refresh(parent);
        return classificationClass;
    }

    protected ClassAttributeAssignmentModel createClassificationAttributeAndAssignment(ClassificationClassModel parent, String name, boolean multiValue) {
        ClassificationAttributeModel classificationAttribute = modelService.create(ClassificationAttributeModel.class);
        classificationAttribute.setName(name);
        classificationAttribute.setDescription(parent.getDescription() + "/" + name);
        classificationAttribute.setCode(parent.getCode() + "/" + name);
        classificationAttribute.setSystemVersion(parent.getCatalogVersion());
        modelService.save(classificationAttribute);

        ClassAttributeAssignmentModel classAttributeAssignment = modelService.create(ClassAttributeAssignmentModel.class);
        classAttributeAssignment.setClassificationClass(parent);
        classAttributeAssignment.setClassificationAttribute(classificationAttribute);
        classAttributeAssignment.setSystemVersion(parent.getCatalogVersion());
        classAttributeAssignment.setMultiValued(multiValue);
        modelService.save(classAttributeAssignment);
        modelService.refresh(parent);

        return classAttributeAssignment;
    }

    protected List<String> getAllPropertyNames(JsonObject jsonObject) {
        return jsonObject.entrySet().stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    protected Set<String> getAllAttributesToProcess() {
        Set<String> set = new HashSet<>();
        set.addAll(primitiveStyleAttributes);
        set.addAll(objectStyleAttributes);
        set.addAll(listStyleAttributes);
        set.addAll(structuredAttributes);
        return set;
    }

    @Required
    public void setClassificationService(ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Required
    public void setPrimitiveStyleAttributes(Set<String> primitiveStyleAttributes) {
        this.primitiveStyleAttributes = primitiveStyleAttributes;
    }

    @Required
    public void setObjectStyleAttributes(Set<String> objectStyleAttributes) {
        this.objectStyleAttributes = objectStyleAttributes;
    }

    @Required
    public void setListStyleAttributes(Set<String> listStyleAttributes) {
        this.listStyleAttributes = listStyleAttributes;
    }

    @Required
    public void setStructuredAttributes(Set<String> structuredAttributes) {
        this.structuredAttributes = structuredAttributes;
    }

    @Required
    public void setIgnoredAttributes(Set<String> ignoredAttributes) {
        this.ignoredAttributes = ignoredAttributes;
    }
}
