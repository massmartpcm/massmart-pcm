package za.co.massmart.core.file.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OnecomMediaCsvRow {

    private String itemNumber;
    private String barcode;
    private String brand;
    private String description;


    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
