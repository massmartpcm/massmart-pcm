package za.co.massmart.core.classification.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public interface ProductClassificationImportService {

    /**
     * Imports a classification structure for a single product from an external service.
     *
     * @param ean                                  the EAN (barcode) of the product whose classifications should be retrieved
     * @param catalogVersion                       the catalog version of the product to be modified
     * @param clientId                             the client ID to use for authentication at the external service
     * @param clientAuthentication                 the authentication token to use for authentication at the external service
     * @param externalServiceSingleProductEndpoint the endpoint at which to access the external service
     * @param rootClassificationClass              the root classification class in Hybris in which product classifications should be created
     */
    void importClassificationsForSingleProduct(String ean, CatalogVersionModel catalogVersion, String clientId, String clientAuthentication, String externalServiceSingleProductEndpoint,
                                               ClassificationClassModel rootClassificationClass);

    /**
     * Imports a classification structure from an external service.
     *
     * @param catalogVersionModel            the catalogVersion that will be used to filter the products
     * @param clientId                       the client ID to use for authentication at the external service
     * @param clientAuthentication           the authentication token to use for authentication at the external service
     * @param startDate                      the start date for the import run
     * @param endDate                        the end date for the import run
     * @param externalServiceTimeZone        the timezone used by the external service, which is used in date calculations to
     *                                       adjust the desired start date
     * @param externalServiceEndpoint        the endpoint at which to access the external service
     * @param externalServicePageAddition    an addition to the endpoint URL in order to facilitate direct access to pages
     * @param numberOfDownloadThreads        the number of threads to spawn in order to download the classifications
     * @param rootClassificationClass        the root classification class in Hybris in which product classifications should be created
     * @param replaceExistingClassifications <tt>true</tt> if the existing classification structure should be destroyed before the import,
     *                                       otherwise <tt>false</tt>
     * @return <tt>true</tt> if the import succeeded entirely; otherwise <tt>false</tt>
     */
    boolean importClassifications(CatalogVersionModel catalogVersionModel, String clientId, String clientAuthentication, DateTime startDate, DateTime endDate,
                                  DateTimeZone externalServiceTimeZone, String externalServiceEndpoint, String externalServicePageAddition, int numberOfDownloadThreads,
                                  ClassificationClassModel rootClassificationClass, boolean replaceExistingClassifications);
}
