package za.co.massmart.core.product.services.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import za.co.massmart.core.product.dao.impl.MassmartProductDao;
import za.co.massmart.core.product.services.ProductService;

import java.util.List;

public class DefaultProductService extends de.hybris.platform.product.impl.DefaultProductService implements ProductService {
    private static final Logger LOG = Logger.getLogger(DefaultProductService.class);


    private MassmartProductDao productDao;

    @Override
    public List<MediaContainerModel> getMediaContainers(final ProductModel product) {
        return product.getGalleryImages();
    }


    @Override
    public ProductModel fetchProductForBarcode(CatalogVersionModel catalogVersion, String barcode) {

        List<ProductModel> products = getProductDao().findProductsByBarcode(catalogVersion, barcode);

        if (!products.isEmpty() && products.size() > 0) {
            return products.get(0);
        }
        return null;
    }


    @Required
    public void setProductDao(final MassmartProductDao productDao)
    {
        this.productDao = productDao;
    }

    /**
     * @return the productDao
     */
    protected MassmartProductDao getProductDao()
    {
        return productDao;
    }

}
