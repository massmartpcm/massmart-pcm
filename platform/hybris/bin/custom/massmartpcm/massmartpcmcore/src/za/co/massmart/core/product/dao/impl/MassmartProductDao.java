package za.co.massmart.core.product.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MassmartProductDao extends DefaultProductDao implements za.co.massmart.core.product.dao.ProductDao {


    public MassmartProductDao(final String typecode)
    {
        super(typecode);
    }

    public List<ProductModel> findProductsByBarcode(final CatalogVersionModel catalogVersion, final String barcode)
    {

        final Map parameters = new HashMap();
        parameters.put(ProductModel.SAPEAN, barcode);
        parameters.put(ProductModel.CATALOGVERSION, catalogVersion);

        return find(parameters);
    }

}
