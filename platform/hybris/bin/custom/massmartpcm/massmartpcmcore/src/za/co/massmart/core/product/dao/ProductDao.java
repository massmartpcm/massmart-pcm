package za.co.massmart.core.product.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface ProductDao extends de.hybris.platform.product.daos.ProductDao {
    public List<ProductModel> findProductsByBarcode(CatalogVersionModel catalogVersion, String barcode);
}
