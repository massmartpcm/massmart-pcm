package za.co.massmart.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.massmart.core.classification.service.ProductClassificationImportService;
import za.co.massmart.core.model.ProductClassificationsImportCronJobModel;

public class ProductClassificationsImportJob extends AbstractJobPerformable<ProductClassificationsImportCronJobModel> {

    private static final Logger log = LoggerFactory.getLogger(ProductClassificationsImportJob.class);

    private static final DateTime EPOCH_DATE = new DateTime(0);

    private ProductClassificationImportService productClassificationImportService;

    /**
     * Imports products, using configuration defined in a specified <tt>ProductClassificationsImportCronJobModel</tt>.
     *
     * The time period for the run is determined by the cron job's <tt>lastProcessedDate</tt>. If <tt>null</tt>,
     * the beginning of the run is the Unix epoch.
     *
     * @param cronJob the cron job containing the configuration to use for the import.
     * @return the result of the job
     */
    @Override
    public PerformResult perform(ProductClassificationsImportCronJobModel cronJob) {

        try {
            ServicesUtil.validateParameterNotNull(cronJob.getClientAuthentication(), "clientAuthentication is required");
            ServicesUtil.validateParameterNotNull(cronJob.getClientId(), "clientId is required");
            ServicesUtil.validateParameterNotNull(cronJob.getNumberOfDownloadThreads(), "numberOfDownloadThreads is required");
            ServicesUtil.validateParameterNotNull(cronJob.getRootClassificationClass(), "rootClassificationClass is required");
            ServicesUtil.validateParameterNotNull(cronJob.getSiloEndpoint(), "siloEndpoint is required");
            ServicesUtil.validateParameterNotNull(cronJob.getSiloEndpointPageAddition(), "siloEndpointPageAddition is required");
            ServicesUtil.validateParameterNotNull(cronJob.getSiloTimeZoneId(), "siloTimeZoneId is required");
            ServicesUtil.validateParameterNotNull(cronJob.isReplaceExistingClassifications(), "replaceExistingClassifications is required");

            DateTime startDate = cronJob.getStartDate() != null ? new DateTime(cronJob.getStartDate()) : cronJob.getLastProcessedDate() != null ? new DateTime(cronJob.getLastProcessedDate()) : EPOCH_DATE;
            DateTime endDate = cronJob.getEndDate() != null ? new DateTime(cronJob.getEndDate()) : new DateTime();

            boolean success = productClassificationImportService.importClassifications(cronJob.getCatalogVersion(), cronJob.getClientId(), cronJob.getClientAuthentication(), startDate, endDate,
                    DateTimeZone.forID(cronJob.getSiloTimeZoneId()), cronJob.getSiloEndpoint(), cronJob.getSiloEndpointPageAddition(), cronJob.getNumberOfDownloadThreads(), cronJob.getRootClassificationClass(),
                    cronJob.isReplaceExistingClassifications());

            cronJob.setLastProcessedDate(endDate.toDate());
            modelService.save(cronJob);

            return success ? new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED) : new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        } catch (Exception e) {
            log.error("Product classification import failed", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }
    }

    public void setProductClassificationImportService(ProductClassificationImportService productClassificationImportService) {
        this.productClassificationImportService = productClassificationImportService;
    }
}
