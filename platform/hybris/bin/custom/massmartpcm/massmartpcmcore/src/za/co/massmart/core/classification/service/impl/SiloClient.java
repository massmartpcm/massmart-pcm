package za.co.massmart.core.classification.service.impl;

import com.google.gson.*;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;
import za.co.massmart.core.classification.impl.model.SiloPage;
import za.co.massmart.core.classification.impl.model.SiloProduct;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class SiloClient {

    private Logger logger = LoggerFactory.getLogger(SiloClient.class);

    private DateTimeFormatter siloDateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    private DateTimeZone siloTimeZone;
    private String siloEndpoint;
    private String siloEndpointPageAddition;

    public SiloClient(String siloEndpoint) {
        this(null, siloEndpoint, null);
    }

    public SiloClient(DateTimeZone siloTimeZone, String siloEndpoint, String siloEndpointPageAddition) {
        this.siloTimeZone = siloTimeZone;
        this.siloEndpoint = siloEndpoint;
        this.siloEndpointPageAddition = siloEndpointPageAddition;
    }

    /**
     * Retrieves a single page of Silo product classification data. This method
     * makes modifications to the import start and end dates to account for the fact that
     * Silo does not allow us to specify hours than 00h00. It expands the range to ensure that
     * no classifications are inadvertently lost.
     *
     * @param clientId            the client ID to use for authentication
     * @param authenticationToken the authentication token to use
     * @param startTime           the start date of the page range in which this page should be
     *                            retrieved, which may be adjusted
     * @param endTime             the end date of the page range in which this page should be
     *                            retrieved, which may be adjusted
     * @param page                the number of the page to retrieve
     * @return a representation of the page retrieved
     */
    public SiloPage getPage(String clientId, String authenticationToken, DateTime startTime, DateTime endTime, int page) {
        ServicesUtil.validateParameterNotNullStandardMessage("siloEndpoint ", siloEndpoint);
        ServicesUtil.validateParameterNotNullStandardMessage("siloTimeZone ", siloTimeZone);
        ServicesUtil.validateParameterNotNullStandardMessage("siloEndpointPageAddition ", siloEndpointPageAddition);

        DateTime requestedSiloStart = startTime.toDateTime(siloTimeZone);
        DateTime actualSiloStart = requestedSiloStart.withTimeAtStartOfDay();

        DateTime requestedSiloEnd = endTime.toDateTime(siloTimeZone);
        DateTime actualSiloEnd = requestedSiloEnd.withTimeAtStartOfDay().plus(Days.ONE);

        String url = siloEndpoint;
        if (page > 1) {
            url += siloEndpointPageAddition;
        }

        logger.info("Request URL = {}",(new UriTemplate(url)).expand(clientId, authenticationToken, siloDateTimeFormatter.print(actualSiloStart), siloDateTimeFormatter.print(actualSiloEnd), page));

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class, clientId, authenticationToken, siloDateTimeFormatter.print(actualSiloStart), siloDateTimeFormatter.print(actualSiloEnd), page);

        try {
            return new SiloPage((JsonObject) new JsonParser().parse(responseEntity.getBody()));
        } catch (JsonSyntaxException | ClassCastException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param clientId            the client ID to use for authentication
     * @param clientAuthentication the authentication token to use
     * @param startDate           the start date
     * @param endDate             the end date
     * @param numberOfDownloadThreads the number of download threads
     * @return
     */
    public List<SiloProduct> getProductList(String clientId, String clientAuthentication, DateTime startDate, DateTime endDate,
                                            int numberOfDownloadThreads) {

        // Get the first page
        SiloPage firstPage = getPage(clientId, clientAuthentication, startDate, endDate, 1);
        int numberOfPages = firstPage.getNumberOfPages();

        logger.info("Silo import service found " + numberOfPages + " pages");

        logger.info("Downloaded first page");

        // Create the pool of executors
        ExecutorService downloadService = Executors.newFixedThreadPool(numberOfDownloadThreads);

        // Create the callables to perform the downloads
        List<Callable<SiloPage>> callables = new ArrayList<>();
        for (int i = 2; i <= numberOfPages; i++) {
            callables.add(new GetSiloPageCallable(logger, this, clientAuthentication, clientId, startDate, endDate, i));
        }

        // Start downloading page 2 to numberOfPages
//        List<Future<SiloPage>> siloPageFutures;
//        try {
//            siloPageFutures = downloadService.invokeAll(callables);
//        } catch (InterruptedException e) {
//            logger.error("Failed to download Silo pages", e);
//            throw new RuntimeException("Failed to download Silo pages", e);
//        }

        // Add the downloaded data to a list
        List<SiloProduct> siloProducts = new ArrayList<>();
        siloProducts.addAll(firstPage.getProducts());

        // Add the downloaded data to a list
//        for (Future<SiloPage> siloPageFuture : siloPageFutures) {
//            try {
//                siloProducts.addAll(siloPageFuture.get().getProducts());
//            } catch (InterruptedException | ExecutionException e) {
//                throw new RuntimeException(e);
//            }
//        }

        int totalList = siloProducts.size();
        logger.info("Silo import service found " + totalList + " total products");

        // Remove any records that do not have barcodes - we can't process those.
        filterOutProductsWithMissingBarcodes(siloProducts);
        logger.info("Processing " + siloProducts.size() + " products with barcodes");

        return siloProducts;
    }

    /**
     * Retrieves classification data for a single product.
     *
     * @param clientId            the client ID to use for authentication
     * @param authenticationToken the authentication token to use
     * @param ean                 the EAN (barcode) of the product whose classifications should be retrieved
     * @return a representation of the product retrieved, or <tt>null</tt> if no product
     * could be found matching the EAN
     */
    public SiloProduct getProduct(String clientId, String authenticationToken, String ean) {
        ServicesUtil.validateParameterNotNullStandardMessage("siloEndpoint ", siloEndpoint);

        logger.info("Request URL = {}",(new UriTemplate(siloEndpoint)).expand(ean, clientId, authenticationToken));

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(siloEndpoint, String.class, ean, clientId, authenticationToken);

        JsonObject productJson = (JsonObject) new JsonParser().parse(responseEntity.getBody());
        return representsNotFound(productJson) ? null : new SiloProduct(productJson.getAsJsonObject("product"));
    }

    /**
     * Retrieves the images for a single product.
     *
     * @param clientId            the client ID to use for authentication
     * @param authenticationToken the authentication token to use
     * @param barcode             the barcode of the product for which images should be retrieved
     * @return                    a representation of the product retrieved, or <tt>null</tt> if no product could be found matching the EAN
     */
    public SiloProduct getProductImages(String clientId, String authenticationToken, String barcode) {
        ServicesUtil.validateParameterNotNullStandardMessage("siloEndpoint", siloEndpoint);

        logger.info("Request URL = {}",(new UriTemplate(siloEndpoint)).expand(barcode, clientId, authenticationToken));

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(siloEndpoint, String.class, barcode, clientId, authenticationToken);

        JsonObject productJson = (JsonObject) new JsonParser().parse(responseEntity.getBody());
        // Need to pass barcode in as a constructor argument because the the json object does not contain it.
        return representsNotFound(productJson) ? null : new SiloProduct(barcode, productJson.getAsJsonObject("product"));
    }

    private boolean representsNotFound(JsonObject productJson) {
        /*This is checking for the way that Silo represents a not found result for a
        product, which is: {"product":["false"]} */
        if (productJson.has("product")) {
            JsonElement productElement = productJson.get("product");
            if (productElement.isJsonArray()) {
                JsonArray valuesArray = productElement.getAsJsonArray();
                if (valuesArray.size() == 1 && valuesArray.get(0).isJsonPrimitive() && "false".equals(valuesArray.get(0).getAsString())) {
                    return true;
                }
            } else {
                if (productElement.isJsonPrimitive()) {
                    return true;
                }
            }
        }
        return false;
    }

    private void filterOutProductsWithMissingBarcodes(Collection<SiloProduct> products) {
        products.removeIf(product -> StringUtils.isEmpty(product.getBarcode()));
    }

    private static class GetSiloPageCallable implements Callable<SiloPage> {

        private String auth;
        private String client;
        private DateTime startDate;
        private DateTime endDate;
        private int page;
        private SiloClient siloClient;
        private Logger logger;

        public GetSiloPageCallable(Logger logger, SiloClient siloClient, String auth, String client, DateTime startDate, DateTime endDate, int page) {
            this.logger = logger;
            this.siloClient = siloClient;
            this.auth = auth;
            this.client = client;
            this.startDate = startDate;
            this.endDate = endDate;
            this.page = page;
        }

        @Override
        public SiloPage call() throws Exception {
            SiloPage siloPage = siloClient.getPage(client, auth, startDate, endDate, page);
            logger.info("Downloaded page " + page);
            return siloPage;
        }
    }

}
