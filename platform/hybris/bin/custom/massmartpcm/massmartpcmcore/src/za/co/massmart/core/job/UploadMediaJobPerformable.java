package za.co.massmart.core.job;

import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.io.FilenameUtils;
import za.co.massmart.core.file.FileProcessor;
import za.co.massmart.core.jalo.UploadFileCronJob;
import za.co.massmart.core.model.UploadFileCronJobModel;

import org.apache.log4j.Logger;
import za.co.massmart.core.util.DateUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class UploadMediaJobPerformable extends AbstractJobPerformable<UploadFileCronJobModel> {

    private static final Logger LOG = Logger.getLogger(UploadFileCronJob.class.getName());
    private static final String YYYY_MM_DD_T_HH_MM = "yyyy-MM-dd'T'HH:mm";

    @Override
    public PerformResult perform(UploadFileCronJobModel model) {

        LOG.info("Starting " + getClass().getSimpleName());
        try {
            LOG.info("Starting UploadFileJob filename(" + model.getFileName() + ") source dir(" +
                    model.getFilePath() + ") archive dir(" + model.getOutputPath() + ")");

            PerformResult performResult = consumeFile(model);

            LOG.info("Completed " + getClass().getSimpleName());
            return performResult;
        } catch (Exception e) {
            LOG.error("Exception occurred during " + getClass().getSimpleName(), e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }
    }

    private PerformResult consumeFile(UploadFileCronJobModel model) throws Exception {
        File resourceDirectory = new File(model.getFilePath());
        if (resourceDirectory.isDirectory()) {

            List<File> files = Arrays.asList(resourceDirectory.listFiles());

            for (File file : files) {

                if (clearAbortRequestedIfNeeded(model) || model.getStatus().equals(CronJobStatus.ABORTED)) {
                    LOG.info("Aborted UploadFileJob with code=" + model.getCode());
                    return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
                }

                if ((!(file.isDirectory())) && ((file.getName().contains(model.getFileName())))) {

                    InputStream inputStream = new FileInputStream(file);
                    FileProcessor fileProcessor = (FileProcessor) Registry.getApplicationContext().getBean(model.getFileProcessorBean());
                    fileProcessor.setImportPath(model.getFilePath());
                    LOG.info("Starting processing file processor " + fileProcessor.getClass().getName());
                    boolean isProcessingFileSuccess = fileProcessor.processFile(inputStream,model.getCode(), file.getName());

                    if (!isProcessingFileSuccess) {
                        // Move the file to error directory
                        errorFileProcessing(file, model.getErrorPath(), FilenameUtils.getExtension(file.getName()));
                        return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
                    } else {
                        // Sucessfully processed the file, archive the file
                        archiveFileAfterProcessing(file, model.getOutputPath(), model.getErrorPath(), FilenameUtils.getExtension(file.getName()));
                    }
                }
            }
        }

        LOG.info("Completed UploadFileJob filename(" + model.getFileName() + ") source dir(" +
                model.getFilePath() + ") archive dir(" + model.getOutputPath() + ")");

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private void archiveFileAfterProcessing(File file, String outputPath, String errorPath, String extension) {

        String processedDate = DateUtils.formatDate(new Date(), YYYY_MM_DD_T_HH_MM);
        String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
        String processedFileName = fileNameWithOutExt + "-" + processedDate + extension;
        String errorMessage = "Unable to archive file " + file.getName();

        try {
            if (file.renameTo(new File(outputPath + processedFileName))) {
                LOG.info("File archived successfully from " + file.getName() + " to " + processedFileName);
            } else {
                LOG.error(errorMessage);
                throw new Exception(errorMessage);
            }
        } catch (Exception e) {
            LOG.error(errorMessage, e);
            errorFileProcessing(file, errorPath, extension);
        }
    }


    private void errorFileProcessing(File file, String errorPath, String extension) {
        // update filename with date and move to error folder
        String processedDate = DateUtils.formatDate(new Date(), YYYY_MM_DD_T_HH_MM);
        String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
        String processedFileName = fileNameWithOutExt + "-" + processedDate + extension;
        String errorMessage = "Unable to move file (" + file.getName() + ") to error directory";

        try {
            if (file.renameTo(new File(errorPath + processedFileName))) {
                LOG.info("File moved to error folder " + file.getName() + " to " + processedFileName);
            } else {
                LOG.error(errorMessage);
            }
        } catch (Exception e) {
            LOG.error(errorMessage, e);
        }
    }

    @Override
    public boolean isAbortable() {
        return true;
    }

}