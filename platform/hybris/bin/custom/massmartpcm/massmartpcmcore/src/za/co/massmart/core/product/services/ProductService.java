package za.co.massmart.core.product.services;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface ProductService extends de.hybris.platform.product.ProductService {

    List<MediaContainerModel> getMediaContainers(final ProductModel product);

    /**
     * fetch product for barcode
     *
     * @param catalogVersion
     * @param barcode
     * @return
     */
    ProductModel fetchProductForBarcode(CatalogVersionModel catalogVersion, String barcode);
}
